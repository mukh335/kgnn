from abc import ABC

import numpy as np
import torch
import torch.nn as nn
from torch.autograd import Variable

from data import denormalize, normalize
from utils import load_data
from torch_geometric.nn.conv import MessagePassing

class RelationEncoder(nn.Module):
    def __init__(self, input_size, hidden_size, output_size):
        super(RelationEncoder, self).__init__()

        self.model = nn.Sequential(
            nn.Linear(input_size, hidden_size),
            nn.ReLU(),
            nn.Linear(hidden_size, output_size),
            nn.ReLU()
        )

    def forward(self, x):
        """
        Args:
            x: [n_relations, input_size]
        Returns:
            [n_relations, output_size]
        """
        return self.model(x)

class ParticleEncoder(nn.Module):
    def __init__(self, input_size, hidden_size, output_size):
        super(ParticleEncoder, self).__init__()

        self.model = nn.Sequential(
            nn.Linear(input_size, hidden_size),
            nn.ReLU(),
            nn.Linear(hidden_size, output_size),
            nn.ReLU()
        )

    def forward(self, x):
        """
        Args:
            x: [n_particles, input_size]
        Returns:
            [n_particles, output_size]
        """
        return self.model(x)


class Propagator(nn.Module):
    def __init__(self, input_size, output_size, residual=False):
        super(Propagator, self).__init__()

        self.residual = residual

        self.linear = nn.Linear(input_size, output_size)
        self.relu = nn.ReLU()

    def forward(self, x, res=None):
        """
        Args:
            x: [n_relations/n_particles, input_size]
        Returns:
            [n_relations/n_particles, output_size]
        """
        if self.residual:
            x = self.relu(self.linear(x) + res)
        else:
            x = self.relu(self.linear(x))

        return x

class ParticlePredictor(nn.Module):
    def __init__(self, input_size, hidden_size, output_size):
        super(ParticlePredictor, self).__init__()

        self.linear_0 = nn.Linear(input_size, hidden_size)
        self.linear_1 = nn.Linear(hidden_size, output_size)
        self.relu = nn.ReLU()

    def forward(self, x):
        """
        Args:
            x: [n_particles, input_size]
        Returns:
            [n_particles, output_size]
        """
        x = self.relu(self.linear_0(x))

        return self.linear_1(x)


class PropagationNetwork(MessagePassing):
    def __init__(self, aggr: str = 'add', args, input_particle_dim=None, input_relation_dim=None, output_dim=None, action=True, tanh=False,
                 residual=False, use_gpu=False, **kwargs):

        super(PropagationNetwork, self).__init__(aggr=aggr, **kwargs)
        self.args = args
        self.action = action

        if input_particle_dim is None:
            input_particle_dim = args.attr_dim + args.state_dim
            input_particle_dim += args.action_dim if action else 0

        if input_relation_dim is None:
            input_relation_dim = args.relation_dim + args.state_dim

        nf_particle = args.nf_particle
        nf_relation = args.nf_relation
        nf_effect = args.nf_effect

        self.nf_effect = args.nf_effect

        self.use_gpu = use_gpu
        self.residual = residual

        # (1) state
        self.obj_encoder = ParticleEncoder(input_particle_dim, nf_particle, nf_effect)

        # (1) state receiver (2) state_diff
        self.relation_encoder = RelationEncoder(input_relation_dim, nf_relation, nf_relation)

        # (1) relation encode (2) sender effect (3) receiver effect
        self.relation_propagator = Propagator(nf_relation + 2 * nf_effect, nf_effect)

        # (1) particle encode (2) particle effect
        self.particle_propagator = Propagator(2 * nf_effect, nf_effect, self.residual)

        # rigid predictor
        # (1) particle encode (2) set particle effect

        self.particle_predictor = ParticlePredictor(nf_effect, nf_effect, output_dim)

        if tanh:
            self.particle_predictor = nn.Sequential(
                self.particle_predictor, nn.Tanh()
            )

    def forward(self, x: Union[Tensor, OptPairTensor], edge_index, edge_attr = rel_attrs):
        # x has shape [N, in_channels]
        # edge_index has shape [2, E]
        """
        :param attrs: B x N x attr_dim
        :param states: B x N x state_dim
        :param actions: B x N x action_dim
        :param rel_attrs: B x N x N x relation_dim
        :param pstep: 1 or 2

        x concatenates states, attrs, actions
        rel_attrs plays the role of edge_attr
        x shape will be then [B x N x (state_dim + attr_dim + action_dim)]
        edge_attr : [B x N x N x relation_dim]

        :return:
        """
        B, N = x.size(0), x.size(1) # check command
        
        states = x[:,:,0:state_dim],
        attrs = x[:,:, state_dim: state_dim+attr_dim], 
        actions = x[:,:,state_dim+attr_dim: state_dim+ attr_dim+action_dim],

        out = self.propagate(edge_index, x=x, edge_attr=edge_attr, size=size)

        x_r = x[1]
        if x_r is not None:
            state_r = x_r[]
            attr_r = x_r[]
            action_r = x_r[]

            obj_input_list = [attr_r, state_r]
            if self.action:
                obj_input_list += [action_r]

            tmp = torch.cat(obj_input_list, 2)
            obj_encode = self.obj_encoder(tmp)

            tmp1 = torch.cat([obj_encode, out], 2)
            obj_encode = self.particle_propagator(tmp1)
        
        if last layer:
            obj_prediction = self.particle_predictor(obj_encode)
        else:
            obj_prediction = obj_encode
        return obj_prediction
        

    def message(self, x_i, x_j, edge_index, edge_attr = rel_attrs):

        states_i = x_i[:,:,0:state_dim], states_j = x_j[:,:,0:state_dim], 
        attrs_i = x_i[:,:, state_dim: state_dim+attr_dim], attrs_j = x_j[:,:, state_dim: state_dim+attr_dim], 
        actions_i = x_i[:,:,state_dim+attr_dim: state_dim+ attr_dim+action_dim], actions_j = x_j[:,:,state_dim+attr_dim: state_dim+ attr_dim+action_dim], 
        

        '''encode node i'''
        obj_input_list_i = [attrs_i, states_i]
        if self.action:
            obj_input_list_i += [actions_i]

        tmp = torch.cat(obj_input_list_i, 2)
        obj_encode_i = self.obj_encoder(tmp.reshape(tmp.size(0) * tmp.size(1), tmp.size(2))).reshape(B, N, -1)
        

        '''encode node j'''
        obj_input_list_j = [attrs_j, states_j]
        if self.action:
            obj_input_list_j += [actions_j]

        tmp = torch.cat(obj_input_list_j, 2)
        obj_encode_j = self.obj_encoder(tmp.reshape(tmp.size(0) * tmp.size(1), tmp.size(2))).reshape(B, N, -1)
        
        '''encode edge'''
        # relative states betwenn i and j
        rel_states = states_i[:, :, None, :] - states_j[:, None, :, :]
        
        receiver_attr = attrs_i[:, :, None, :]
        sender_attr = attrs_j[:, None, :, :]

        # concatenate rel_states, rel_attr, sender and receiver node attrs
        tmp = torch.cat([rel_attrs, rel_states, attr_i, attr_j], 3)
        rel_encode = self.relation_encoder(tmp)


        receiver_code = obj_encode_i[:, :, None, :]
        sender_code = obj_encode_j[:, None, :, :]
        tmp1 = torch.cat([rel_encode, receiver_code, sender_code], 3)

        rel_effect = self.relation_propagator(tmp1)


class KoopmanLayer(MessagePassing):

    def __init__(self, nn: Callable, eps: float = 0., train_eps: bool = False,
                 **kwargs):
        kwargs.setdefault('aggr', 'add')
        super(KoopmanLayer, self).__init__(**kwargs)


    def forward(self, x: Union[Tensor, OptPairTensor], edge_index: Adj,
                edge_attr: OptTensor = None, size: Size = None, K,L) -> Tensor:
        """"""
        

        # propagate_type: (x: OptPairTensor, edge_attr: OptTensor)
        out = self.propagate(edge_index, x=x, edge_attr=edge_attr, size=size)

        x_r = x[1]
        if x_r is not None:
            state_r = x_r[]
            action_r = x_r[]
            out += K[i,i]*state_r + L[i,i]*action_r

        return self.nn(out)


    def message(self, x_j: Tensor, edge_index, edge_attr: Tensor, K) -> Tensor:
        state_j = x_j[]
        based on edge_index find K[i,j]
        return K[ij]*state_j



        