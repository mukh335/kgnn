from abc import ABC
import os, time, pickle
import numpy as np
import random
import torch
import torch.nn as nn
from torch.autograd import Variable
import torch.optim as optim
from torch.optim.lr_scheduler import ReduceLROnPlateau
import torch.nn as nn
from progressbar import ProgressBar
from typing import Callable, Union
from torch_geometric import datasets
from torch_geometric.typing import OptPairTensor, Adj, OptTensor, Size
from torch.utils.data import Dataset, DataLoader
from torch import Tensor
import torch.nn.functional as F
from torch_sparse import SparseTensor, matmul
from torch_geometric.nn.conv import MessagePassing
from torch_geometric.datasets import Planetoid
#from data import denormalize, normalize
#from utils import load_data
from torch_geometric.nn.conv import MessagePassing
from utils import count_parameters, Tee, AverageMeter, rand_int, mix_iters, get_flat, print_args, init_stat
from KGNN_distributed_Koopman import *
from data import PhysicsDataset
from data import load_data
from config_kgnn import gen_args
from data_inputs_kgnn import data, args_e, args2_e, args_d, args2_d # important, added this
from data_inputs_kgnn import g_d, Tfp_list, Rfp_list, Tpp_list, Rpp_list, Tup_list, Rup_list, agent_sizes, Adj
import h5py

args = gen_args()
patience = 5
#dataset = Planetoid(root='/tmp/Cora', name='Cora')
#data = dataset[0]
# Creating dummy dataset 
#bs=5
#T=6  # T+1 convention






class Data_states:
    pass 
class Data_g:
    pass 





# n_samples = bs*20
# data.states = torch.rand(n_samples, T, data.n_nodes,data.state_dim)
# data.attr = torch.rand(n_samples, T, data.n_nodes,data.attr_dim)
# data.action = torch.rand(n_samples, T, data.n_nodes,data.action_dim)
# data.edge_attr = torch.rand(n_samples, T, data.n_edges,data.relation_dim)


# def store_data(data_names, data, path):
#     hf = h5py.File(path, 'w')
#     for i in range(len(data_names)):
#         hf.create_dataset(data_names[i], data=data[i])
#     hf.close()

# data_names = ['attrs', 'states', 'actions', 'rel_attrs']
# data_stat = [init_stat(args.attr_dim),
#                              init_stat(args.state_dim),
#                              init_stat(args.action_dim),
#                              init_stat(args.relation_dim)]
# path_stat = os.path.join(args.dataf, 'stat.h5') 
# store_data(data_names, data_stat, path_stat)

os.system('mkdir -p ' + args.outf)
#os.system('mkdir -p ' + args.dataf)
tee = Tee(os.path.join(args.outf, 'train.log'), 'w')


# generate data
datasets = {phase: PhysicsDataset(args, phase) for phase in ['train', 'valid']}
for phase in ['train', 'valid']:
    if args.gen_data:
        datasets[phase].gen_data()
    else:
        datasets[phase].load_data()

if args.gen_data:
    print("Preprocessing data ...")
    os.system('python preprocess_data.py --env ' + args.env)

# print("Preprocessing data ...")
# os.system('python preprocess_data_grid.py --env ' + args.env)

args.stat = datasets['train'].stat


class ShuffledDataset(Dataset):
    def __init__(self,
                 mother_dataset,
                 idx,
                 batch_size):
        self.samples_per_rollout = args.time_step - args.len_seq
        self.mother = mother_dataset
        self.n_rollout = mother_dataset.n_rollout // args.n_splits
        self.idx = idx
        self.prepared_names = ['attrs', 'states', 'actions', 'rel_attrs']
        self.batch_size = batch_size

        self.build_table()

    def __len__(self):
        return self.n_rollout * self.samples_per_rollout

    def build_table(self):
        assert self.n_rollout % args.group_size == 0
        bs = self.batch_size
        num_groups = self.n_rollout // args.group_size

        sample_list = [[] for _ in range(num_groups)]
        for i in range(self.n_rollout):
            for j in range(self.samples_per_rollout):
                gidx = i // args.group_size
                sample_list[gidx].append((i, j))

        '''shuffle sample list'''
        for i in range(num_groups):
            l = sample_list[i]
            random.shuffle(l)

        '''padding samples in the same group such that the size can be divied by the batch size'''
        for i in range(num_groups):
            if len(sample_list[i]) % bs > 0:
                sample_list[i] += sample_list[i][:bs - len(sample_list[i]) % bs]

        '''create batches'''
        batch_list = []
        for i in range(num_groups):
            l = sample_list[i]
            for j in range(len(l) // bs):
                batch_list.append(l[j * bs:j * bs + bs])

        '''merge the batch list to a total sample list'''
        random.shuffle(batch_list)
        total_list = []
        for batch in batch_list:
            total_list += batch
        self.sample_table = total_list

    def __getitem__(self, idx):
        # print('dataset', self.idx, 'sample', idx)
        idx_rollout = self.sample_table[idx][0] + self.n_rollout * self.idx
        idx_timestep = self.sample_table[idx][1]

        # prepare input data
        seq_data = load_data(self.prepared_names, os.path.join(self.mother.data_dir, str(idx_rollout) + '.rollout.h5'))
        seq_data = [d[idx_timestep:idx_timestep + args.len_seq + 1] for d in seq_data]

        # prepare fit data
        fit_idx = rand_int(0, args.group_size - 1)  # new traj idx in group
        fit_idx = fit_idx + idx_rollout // args.group_size * args.group_size  # new traj idx in global
        fit_data = load_data(self.prepared_names, os.path.join(self.mother.data_dir, str(fit_idx) + '.rollout.h5'))

        return seq_data, fit_data


class SubPreparedDataset(Dataset):

    def __init__(self,
                 mother_dataset,
                 idx, ):
        self.samples_per_rollout = args.time_step - args.len_seq
        self.mother = mother_dataset
        self.n_rollout = mother_dataset.n_rollout // args.n_splits
        self.idx = idx
        self.prepared_names = ['attrs', 'states', 'actions', 'rel_attrs']

    def __len__(self):
        return self.n_rollout * self.samples_per_rollout

    def __getitem__(self, idx):
        idx_rollout = idx // self.samples_per_rollout + self.n_rollout * self.idx
        idx_timestep = idx % self.samples_per_rollout

        # prepare input data
        seq_data = load_data(self.prepared_names, os.path.join(self.mother.data_dir, str(idx_rollout) + '.rollout.h5'))
        seq_data = [d[idx_timestep:idx_timestep + args.len_seq + 1] for d in seq_data]

        # prepare fit data
        fit_idx = rand_int(0, args.group_size - 1)  # new traj idx in group
        fit_idx = fit_idx + idx_rollout // args.group_size * args.group_size  # new traj idx in global
        fit_data = load_data(self.prepared_names, os.path.join(self.mother.data_dir, str(fit_idx) + '.rollout.h5'))

        return seq_data, fit_data


def split_dataset(ds):
    assert ds.n_rollout % args.group_size == 0
    assert ds.n_rollout % args.n_splits == 0
    sub_datasets = [ShuffledDataset(mother_dataset=ds, idx=i, batch_size=args.batch_size) for i in range(args.n_splits)]
    return sub_datasets


use_gpu = torch.cuda.is_available()

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
if use_gpu:
    data.edge_index = data.edge_index.to(device)
    #all_buses = all_buses.to(device)
    #all_buses_reverse = all_buses_reverse.to(device)

#use_gpu = False
"""
various number of objects, need mixing datasets
"""

dataloaders = {}
data_n_batches = {}
loaders = {}
for phase in ['train', 'valid']:
    loaders[phase] = [DataLoader(
        dataset=dataset, batch_size=args.batch_size,
        shuffle=False,
        num_workers=args.num_workers, )
        for dataset in split_dataset(datasets[phase])]

    dataloaders[phase] = lambda: mix_iters(iters=[iter(loader) for loader in loaders[phase]])

    num_batches = sum(len(loader) for loader in loaders[phase])
    data_n_batches[phase] = num_batches






# class PretrainData(torch.utils.data.Dataset):
#     def __init__(self, states, attr, action, edge_attr):
#         self.states = states
#         self.attr = attr
#         self.action = action
#         self.edge_attr = edge_attr
    
#     def __len__(self):
#         return len(self.states)
    
#     def __getitem__(self, index):
#         states_idx = self.states[index]
#         attr_idx = self.attr[index]
#         action_idx = self.action[index] 
#         edge_attr_idx =  self.edge_attr[index] 
#         # print(f'X.shape: {X.shape} Y.shape: {Y.shape} masked_pos_at_idx.shape: {masked_pos_at_idx.shape} mask_at_idx.shape:{mask_at_idx.shape}')
#         return states_idx ,attr_idx , action_idx , edge_attr_idx


# args_general.val_split = .15
# args_general.test_split = .15
# args_general.batch_size = bs
# def split_train_val_test(data, args_general):
    
#     states, attr, action, edge_attr = data
#     val_split = float(args_general.val_split) 
#     test_split = float(args_general.test_split)
#     train_split = 1.0 - (test_split+ val_split)
#     num_total_visits = int(states.size(0))
#     num_train = int(num_total_visits*train_split)
#     num_val = int(num_total_visits*val_split)
#     num_test = int(num_total_visits*test_split)
       
#     print("Number of train, val, test ", num_train, num_val, num_test)
#     states_train = states[0:num_train, :]
#     attr_train = attr[0:num_train, :] 
#     action_train = action[0:num_train, :]
#     edge_attr_train = edge_attr[0:num_train, :]
#     training_data = PretrainData(states_train, attr_train, action_train, edge_attr_train)
        
#     states_val = states[num_train: num_train+num_val, :]
#     attr_val = attr[num_train: num_train+num_val, :] 
#     action_val = action[num_train: num_train+num_val, :]
#     edge_attr_val = edge_attr[num_train: num_train+num_val, :]
#     val_data = PretrainData(states_val, attr_val, action_val, edge_attr_val)

#     states_test = states[num_train+num_val:, :]
#     attr_test = attr[num_train+num_val:, :] 
#     action_test = action[num_train+num_val:, :]
#     edge_attr_test = edge_attr[num_train+num_val:, :]
#     test_data = PretrainData(states_test, attr_test, action_test, edge_attr_test)
#     print(states_train.size(), attr_train.size(), action_train.size(), edge_attr_train.size())
#     print(states_val.size(), attr_val.size(), action_val.size(), edge_attr_val.size())
#     print(states_test.size(), attr_test.size(), action_test.size(), edge_attr_test.size())
#     return training_data, val_data, test_data


# seq_data = [data.states, data.attr, data.action, data.edge_attr]
# fit_data= seq_data


# #training_data, val_data, test_data = split_train_val_test(data.states, data.attr, data.action, data.edge_attr, args_general)
# training_data_seq, val_data_seq, test_data_seq = split_train_val_test(seq_data, args_general)

# training_data_fit, val_data_fit, test_data_fit = split_train_val_test(fit_data, args_general)




# #train_sampler = torch.utils.data.Sampler(training_data)
# train_loader_seq = torch.utils.data.DataLoader(training_data_seq, batch_size=int(args_general.batch_size), shuffle=False, num_workers=1, pin_memory=True, drop_last=True)

# val_loader_seq = torch.utils.data.DataLoader(val_data_seq, batch_size=int(args_general.batch_size), shuffle=False, num_workers=1, pin_memory=True, drop_last=True)
# test_loader_seq = torch.utils.data.DataLoader(test_data_seq, batch_size=int(args_general.batch_size), shuffle=False, num_workers=1, pin_memory=True, drop_last=True)


# train_loader_fit = torch.utils.data.DataLoader(training_data_fit, batch_size=int(args_general.batch_size), shuffle=False, num_workers=1, pin_memory=True, drop_last=True)

# val_loader_fit = torch.utils.data.DataLoader(val_data_fit, batch_size=int(args_general.batch_size), shuffle=False, num_workers=1, pin_memory=True, drop_last=True)
# test_loader_fit = torch.utils.data.DataLoader(test_data_fit, batch_size=int(args_general.batch_size), shuffle=False, num_workers=1, pin_memory=True, drop_last=True)







# data_valid = data
# data_valid.states = torch.rand(n_samples, T, data.n_nodes,data.state_dim)
# data_valid.attr = torch.rand(n_samples, T, data.n_nodes,data.attr_dim)
# data_valid.action = torch.rand(n_samples, T, data.n_nodes,data.action_dim)
# data_valid.edge_attr = torch.rand(n_samples, T, data.n_edges,data.relation_dim)



# data_n_batches = {}
# loaders = {}
# for phase in ['train', 'valid']:
#     loaders[phase] = [DataLoader(
#         dataset=dataset, batch_size=args_general.batch_size,
#         shuffle=False,
#         num_workers=args_general.num_workers, )
#         for dataset in split_dataset(datasets[phase])]

#     dataloaders[phase] = lambda: mix_iters(iters=[iter(loader) for loader in loaders[phase]])

#     num_batches = sum(len(loader) for loader in loaders[phase])
#     data_n_batches[phase] = num_batches





#use_gpu = torch.cuda.is_available()
# Compositional Koopman Operator
model = GNNKoopman(args_e, args2_e, args_d, args2_d, action = True, tanh = True, residual=False, use_gpu=use_gpu)

# print model #params
print("model #params: %d" % count_parameters(model))

# if resume from a pretrained checkpoint
# if args_general.resume_epoch >= 0:
#     model_path = os.path.join(args_general.outf, 'net_epoch_%d_iter_%d.pth' % (args_general.resume_epoch, args_general.resume_iter))
#     print("Loading saved ckp from %s" % model_path)
#     model.load_state_dict(torch.load(model_path))

# criterion
criterionMSE = nn.MSELoss()

# optimizer
params = model.parameters()
optimizer = optim.Adam(params, lr=args.lr, betas=(.97, 0.999))
scheduler = ReduceLROnPlateau(optimizer, 'min', factor=0.6, patience=2, verbose=True)

if use_gpu:
    model = model.cuda()
    criterionMSE = criterionMSE.cuda()

#st_epoch = args_general.resume_epoch if args_general.resume_epoch > 0 else 0
best_valid_loss = np.inf

st_epoch = args.resume_epoch if args.resume_epoch > 0 else 0
log_fout = open(os.path.join(args.outf, 'log_st_epoch_%d.txt' % st_epoch), 'w')

datalist, datalist_best = [],[]
t1 = time.time()

# phase = 'train'
# seq_data = [data.attr, data.states, data.action, data.edge_attr]
# fit_data= seq_data

for epoch in range(st_epoch, args.n_epoch):

    Data_input_processing = 0
    Preparation_for_GNNencoding_for_Koopman_time = 0
    GNN_encoder_time_for_koopman = 0
    Preparation_time_koopman_leastsquares = 0
    koopman_leastsquares_time = 0
    Prep_GNN_encoder_forward_prop_time = 0
    GNN_encoder_forward_prop_time = 0
    loss_metric_time = 0
    koopman_forward_simulate_time = 0
    Preparation_decoding_for_AE_time = 0
    GNN_decoding_for_AE_time = 0
    Preparation_GNN_decoding_for_prediction_time = 0
    GNN_decoding_for_prediction_time = 0
    loss_computation_time = 0
    optimizer_time = 0
    

    phases = ['train', 'valid'] if args.eval == 0 else ['valid']

    for phase in phases:
        model.train(phase == 'train')
        meter_loss = AverageMeter()
        meter_loss_metric = AverageMeter()
        meter_loss_ae = AverageMeter()
        meter_loss_pred = AverageMeter()
        meter_fit_error = AverageMeter()
        meter_dist_g = AverageMeter()
        meter_dist_s = AverageMeter()

        bar = ProgressBar(max_value=data_n_batches[phase])

        loader = dataloaders[phase]()

        


        for i, (seq_data, fit_data) in bar(enumerate(loader)):

            start_time = time.time()
            attrs, states, actions, rel_attrs = seq_data
            attrs_2, states_2, actions_2, rel_attrs_2 = fit_data # fit data

            # attrs = attrs[:,0:args.len_seq:2,:,:]
            # states = states[:,0:args.len_seq:2,:,:]
            # actions = actions[:,0:args.len_seq:2,:,:]
            # rel_attrs = rel_attrs[:,0:args.len_seq:2,:,:]

            # attrs_2 = attrs_2[:,0:args.time_step:2,:,:]
            # states_2 = states_2[:,0:args.time_step:2,:,:]
            # actions_2 = actions_2[:,0:args.time_step:2,:,:]
            # rel_attrs_2 = rel_attrs_2[:,0:args.time_step:2,:,:]
            
            
            
            
            # rel_attrs= torch.from_numpy(edge_attr[None,:,:].repeat(rel_attrs.size(1), axis=0)[None,:,:,:].repeat(rel_attrs.size(0), axis=0)).float()
            # rel_attrs_2= torch.from_numpy(edge_attr[None,:,:].repeat(rel_attrs_2.size(1), axis=0)[None,:,:,:].repeat(rel_attrs_2.size(0), axis=0)).float()

            # seq_data = [attrs, states, actions, rel_attrs]
            # fit_data = [attrs_2, states_2, actions_2, rel_attrs_2]


            # print('attrs', attrs.shape)           bs x len_seq x num_obj x attr_dim # node attribute-- power grid buses are the nodes, each node - gen/load/not connected 
            # print('states', states.shape)         bs x len_seq x num_obj x state_dim # 10kx100x68x3 -- voltage mag, freq, voltage angle
            # print('actions', actions.shape)       bs x len_seq x num_obj x action_dim # 10kx100x16x1/10kx100x68x1  remove don't compute B matrix
            # print('rel_attrs', rel_attrs.shape)   bs x len_seq x num_obj x num_obj x rel_dim


            # need to change rel_attr to bs x len_seq x num_edge x rel_dim

            if use_gpu:
                attrs_2, states_2, actions_2, rel_attrs_2 = [x.cuda() for x in fit_data]
            fit_data = [attrs_2, states_2, actions_2, rel_attrs_2]
            Data_input_processing = Data_input_processing +  (time.time() - start_time)
            with torch.set_grad_enabled(phase == 'train'):
                start_time = time.time()
                if use_gpu:
                    attrs, states, actions, rel_attrs = [x.cuda() for x in seq_data]
                data_seq = [attrs, states, actions, rel_attrs]
        

                """
                flatten fit data
                """
                attrs_flat = get_flat(attrs_2)
                states_flat = get_flat(states_2)
                actions_flat = get_flat(actions_2)
                rel_attrs_flat = get_flat(rel_attrs_2)

                # 
                Data_states.states = states_flat
                Data_states.n_nodes = states_2.shape[2]
                Data_states.n_edges = rel_attrs_2.shape[2]
                Data_states.state_dim = states_2.shape[3]
                Data_states.attr_dim = attrs_2.shape[3]
                Data_states.attr = attrs_flat
                Data_states.action_dim = actions_2.shape[3]
                Data_states.action = actions_flat

                

                Data_states.edge_index = data.edge_index  # need to define this globally
                Data_states.x_cat = torch.cat([Data_states.states, Data_states.attr], 2)

                Data_states.relation_dim = rel_attrs_2.shape[3]
                Data_states.edge_attr = rel_attrs_flat

                # create data_flat
                
                T = args.len_seq

                bs = len(attrs)
                Preparation_for_GNNencoding_for_Koopman_time = Preparation_for_GNNencoding_for_Koopman_time +  (time.time() - start_time)

                start_time = time.time()
                g = model.to_g(Data_states)
                GNN_encoder_time_for_koopman = GNN_encoder_time_for_koopman +  (time.time() - start_time)

                start_time = time.time()
                g = g.view(torch.Size([bs, args.time_step]) + g.size()[1:])

                """
                fit A with fit data
                !!! need to force that rel_attrs in one group to be the same !!!
                """
                G_tilde = g[:, :-1]
                H_tilde = g[:, 1:]
                U_left = actions_2[:, :-1]

                G_tilde = get_flat(G_tilde, keep_dim=True)
                H_tilde = get_flat(H_tilde, keep_dim=True)
                U_left = get_flat(U_left, keep_dim=True)

                # G_tilde_ordered = torch.index_select(G_tilde, 2, all_buses)
                # H_tilde_ordered = torch.index_select(H_tilde, 2, all_buses)
                # U_left_ordered = torch.index_select(U_left, 2, all_buses)


                Preparation_time_koopman_leastsquares = Preparation_time_koopman_leastsquares +  (time.time() - start_time)
                start_time = time.time()
                A, B, fit_err = model.system_identify_distributed(G=G_tilde, H=H_tilde, U=U_left,
                                                        rel_attrs=rel_attrs[:1, 0], I_factor=args.I_factor, Adj = Adj, agent_sizes=agent_sizes,
                                                        g_d=g_d, Tfp_list=Tfp_list, Rfp_list=Rfp_list, Tpp_list=Tpp_list, Rpp_list=Rpp_list, Tup_list=Tup_list, Rup_list=Rup_list)
                koopman_leastsquares_time = koopman_leastsquares_time + (time.time() - start_time)

                start_time = time.time()
                model.A = model.A.repeat(bs, 1, 1)
                model.B = model.B.repeat(bs, 1, 1)

                meter_fit_error.update(fit_err.item(), bs)

                """
                forward on sequential data
                """

                attrs_flat = get_flat(attrs)
                states_flat = get_flat(states)
                actions_flat = get_flat(actions)
                rel_attrs_flat = get_flat(rel_attrs)
                
                # edit this
                Data_states.states = states_flat
                Data_states.n_nodes = states_2.shape[2]
                Data_states.n_edges = rel_attrs_2.shape[2]
                Data_states.state_dim = states_2.shape[3]
                Data_states.attr_dim = attrs_2.shape[3]
                Data_states.attr = attrs_flat
                Data_states.action_dim = actions_2.shape[3]
                Data_states.action = actions_flat
                Data_states.edge_index = data.edge_index  # need to define this globally
                Data_states.x_cat = torch.cat([Data_states.states, Data_states.attr], 2)

                Data_states.relation_dim = rel_attrs_2.shape[3]
                Data_states.edge_attr = rel_attrs_flat
                Prep_GNN_encoder_forward_prop_time = Prep_GNN_encoder_forward_prop_time +  (time.time() - start_time)
                start_time = time.time()
                g = model.to_g(Data_states)
                GNN_encoder_forward_prop_time = GNN_encoder_forward_prop_time +  (time.time() - start_time)

                start_time = time.time()
                if args.env == 'Rope':
                    permu = np.random.permutation(bs * (T + 1))
                    split_0 = permu[:bs * (T + 1) // 2]
                    split_1 = permu[bs * (T + 1) // 2:]
                else:
                    permu = np.random.permutation(bs * (T))
                    split_0 = permu[:bs * (T ) // 2]
                    split_1 = permu[bs * (T ) // 2:]

                dist_g = torch.mean((g[split_0] - g[split_1]) ** 2, dim=(1, 2))
                dist_s = torch.mean((states_flat[split_0] - states_flat[split_1]) ** 2, dim=(1, 2))
                scaling_factor = 10
                loss_metric = torch.abs(dist_g * scaling_factor - dist_s).mean()
                loss_metric_time = loss_metric_time + (time.time() - start_time)
                #g = g.view(torch.Size([bs, args.time_step]) + g.size()[1:])
                g = g.view(torch.Size([bs, T+1]) + g.size()[1:])

                """
                rollout 0 -> 1 : T + 1
                
                """
                start_time = time.time()
                #g_ordered = torch.index_select(g, 2, all_buses)
                U_for_pred = actions[:, : T]
                #U_for_pred_ordered = torch.index_select(U_for_pred, 2, all_buses)
                G_for_pred = model.simulate(T=T, g=g[:, 0], u_seq=U_for_pred, rel_attrs=rel_attrs[:, 0])
                #G_for_pred = torch.index_select(G_for_pred_ordered, 2, all_buses_reverse)
                koopman_forward_simulate_time = koopman_forward_simulate_time + (time.time() - start_time)
                ''' rollout time: T // 2 + 1, T '''

                start_time = time.time()
                data_for_ae = [x[:, :T + 1] for x in data_seq]
                data_for_pred = [x[:, 1:T + 1] for x in data_seq]

                #data_for_ae = [x[:, :T -1] for x in data_seq]
                #data_for_pred = [x[:, 1:T ] for x in data_seq]

                # decode state for auto-encoding

                ''' BT x N x 4 '''
                attrs_for_ae_flat = get_flat(data_for_ae[0])
                rel_attrs_for_ae_flat = get_flat(data_for_ae[3])
                gcodes=get_flat(g[:, :T + 1])
                #gcodes=get_flat(g[:, :T ])

                # set Data_g
                Data_g.states = gcodes
                Data_g.n_nodes = states_2.shape[2]
                Data_g.n_edges = rel_attrs_2.shape[2]
                Data_g.state_dim = gcodes.shape[2]
                Data_g.attr_dim = attrs_2.shape[3]
                Data_g.attr = attrs_for_ae_flat
                Data_g.edge_index = data.edge_index  # need to define this globally
                Data_g.x_cat = torch.cat([Data_g.states, Data_g.attr], 2)

                Data_g.relation_dim = rel_attrs_2.shape[3]
                Data_g.edge_attr = rel_attrs_for_ae_flat

                Preparation_decoding_for_AE_time = Preparation_decoding_for_AE_time + (time.time() - start_time)

                start_time = time.time()
                decode_s_for_ae = model.to_s(Data_g)
                GNN_decoding_for_AE_time = GNN_decoding_for_AE_time + (time.time() - start_time)

                # decode state for prediction

                ''' BT x N x 4 '''
                start_time = time.time()
                attrs_for_pred_flat = get_flat(data_for_pred[0])
                rel_attrs_for_pred_flat = get_flat(data_for_pred[3])
                
                
                # set Data_g
                gcodes=get_flat(G_for_pred)
                Data_g.states = gcodes
                Data_g.attr = attrs_for_pred_flat
                Data_g.x_cat = torch.cat([Data_g.states, Data_g.attr], 2)
                Data_g.edge_attr = rel_attrs_for_pred_flat

                Preparation_GNN_decoding_for_prediction_time = Preparation_GNN_decoding_for_prediction_time + (time.time() - start_time)

                start_time = time.time()
                decode_s_for_pred = model.to_s(Data_g)
                GNN_decoding_for_prediction_time = GNN_decoding_for_prediction_time +  (time.time() - start_time)

                start_time = time.time()

                loss_auto_encode = F.l1_loss(
                    decode_s_for_ae, states[:, :T + 1].reshape(decode_s_for_ae.shape))
                loss_prediction = F.l1_loss(
                    decode_s_for_pred, states[:, 1:].reshape(decode_s_for_pred.shape))

                # loss_auto_encode = F.l1_loss(
                #     decode_s_for_ae, states[:, :T].reshape(decode_s_for_ae.shape))
                # loss_prediction = F.l1_loss(
                #     decode_s_for_pred, states[:, 1:].reshape(decode_s_for_pred.shape))    

                loss = loss_auto_encode + loss_prediction + loss_metric * .3

                meter_loss_metric.update(loss_metric.item(), bs)
                meter_loss_ae.update(loss_auto_encode.item(), bs)
                meter_loss_pred.update(loss_prediction.item(), bs)

                meter_dist_g.update(dist_g.mean().item(), bs)
                meter_dist_s.update(dist_s.mean().item(), bs)
                loss_computation_time = loss_computation_time +  (time.time() - start_time)

            '''prediction loss'''
            meter_loss.update(loss.item(), bs)

            if phase == 'train':
                start_time = time.time()
                optimizer.zero_grad()
                # for param in model.parameters():
                #     param.grad = None
                loss.backward()
                nn.utils.clip_grad_norm_(model.parameters(), args.grad_clip)
                optimizer.step()
                optimizer_time = optimizer_time +  (time.time() - start_time)
                #print(i, flush=True)

            if i % args.log_per_iter == 0:
                log = '%s [%d/%d][%d] Loss: %.6f (%.6f), sysid_error: %.6f (%.6f), loss_ae: %.6f (%.6f), loss_pred: %.6f (%.6f), ' \
                      'loss_metric: %.6f (%.6f)' % (
                    phase, epoch, args.n_epoch, i,
                    loss.item(), meter_loss.avg,
                    fit_err.item(), meter_fit_error.avg,
                    loss_auto_encode.item(), meter_loss_ae.avg,
                    loss_prediction.item(), meter_loss_pred.avg,
                    loss_metric.item(), meter_loss_metric.avg,
                      )
                data_log = [phase, epoch, i , loss.item(), meter_loss.avg,
                    fit_err.item(), meter_fit_error.avg,
                    loss_auto_encode.item(), meter_loss_ae.avg,
                    loss_prediction.item(), meter_loss_pred.avg,
                    loss_metric.item(), meter_loss_metric.avg,]
                datalist.append(data_log)

                print()
                print(log)

                log_fout.write(log + '\n')
                log_fout.flush()

            if phase == 'train' and i % args.ckp_per_iter == 0:
                torch.save(model.state_dict(), '%s/net_epoch_%d_iter_%d.pth' % (args.outf, epoch, i))

        log = '%s [%d/%d] Loss: %.4f, Best valid: %.4f' % (phase, epoch, args.n_epoch, meter_loss.avg, best_valid_loss)
        print(log)
        
        log_fout.write(log + '\n')
        log_fout.flush()

        if phase == 'valid' and not args.eval:
            scheduler.step(meter_loss.avg)
            if meter_loss.avg < best_valid_loss:
                best_valid_loss = meter_loss.avg
                torch.save(model.state_dict(), '%s/net_best.pth' % (args.outf))
                wait = patience
                print("Found better val_loss.", flush=True)
            else:
                wait -= 1
                print(f"Wait {wait} more epochs to check.", flush=True)
            if wait == 0:
                break

    print("Data_input_processing (s) per epoch", Data_input_processing,flush=True)
    print("Preparation_for_GNNencoding_for_Koopman_time (s) per epoch", Preparation_for_GNNencoding_for_Koopman_time, flush=True)
    print("GNN_encoder_time_for_koopman (s) per epoch", GNN_encoder_time_for_koopman,flush=True)
    print("Preparation_time_koopman_leastsquares (s) per epoch", Preparation_time_koopman_leastsquares,flush=True)
    print("koopman_leastsquares_time (s) per epoch", koopman_leastsquares_time,flush=True)
    print("Prep_GNN_encoder_forward_prop_time (s) per epoch", Prep_GNN_encoder_forward_prop_time,flush=True)
    print("GNN_encoder_forward_prop_time (s) per epoch", GNN_encoder_forward_prop_time,flush=True)
    print("loss_metric_time (s) per epoch", loss_metric_time,flush=True)
    print("koopman_forward_simulate_time (s) per epoch", koopman_forward_simulate_time,flush=True)
    print("Preparation_decoding_for_AE_time (s) per epoch", Preparation_decoding_for_AE_time,flush=True)
    print("GNN_decoding_for_AE_time (s) per epoch", GNN_decoding_for_AE_time,flush=True)
    print("Preparation_GNN_decoding_for_prediction_time (s) per epoch", Preparation_GNN_decoding_for_prediction_time,flush=True)
    print("GNN_decoding_for_prediction_time_time (s) per epoch", GNN_decoding_for_prediction_time,flush=True)
    print("loss_computation_time (s) per epoch", loss_computation_time,flush=True)
    print("Optimizer time (s) per epoch", optimizer_time,flush=True)


t2 = time.time()
print('Total training and valid time:', t2 - t1)
with open('training_log_plots/train_log.pickle', 'wb') as f:
    pickle.dump(datalist, f)
with open('training_log_plots/train_log_best.pickle', 'wb') as f:
    pickle.dump(datalist_best, f)
log_fout.close()

            

        