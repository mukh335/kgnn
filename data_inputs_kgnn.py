from config_kgnn import gen_args
import numpy as np
import torch
#import helper_fcns 
from time import process_time
import csv
import pandas as pd
import scipy.io as sio

args = gen_args()
# args.env = 'Oscillator'
class data:
    pass 

# state shape -- torch.Size([8, 65, 5, 4])

if args.env == 'Rope':
    data.state_dim = 4
    data.n_nodes = 6
    data.n_edges = 36
    data.attr_dim = 2
    data.action_dim = 1
    data.relation_dim = 8
    # data.states = torch.rand(bs, T, data.n_nodes,data.state_dim)
    # data.attr = torch.rand(bs, T, data.n_nodes,data.attr_dim)
    # data.action = torch.rand(bs, T, data.n_nodes,data.action_dim)
    # data.edge_attr = torch.rand(bs, T, data.n_edges,data.relation_dim)


    edge_index = np.array([[],[]])
    for i in range(data.n_nodes):
        for j in range(data.n_nodes):
            #if rel_attrs[i,j].any():
            edge_index = np.append(edge_index, np.array([[i], [j]]), axis=1)
    
    data.edge_index = torch.from_numpy(edge_index).long()
    bs = 1 # due to flatening inside Koopman computation function
    Adj = np.array([[0,1,0,0,0,0],[1,0,1,0,0,0],[0,1,0,1,0,0],[0,0,1,0,1,0],[0,0,0,1,0,1],[0,0,0,0,1,0]])
    N=data.n_nodes
    m=args.g_dim
    p=data.action_dim = 1
    agent_sizes = np.ones((1,N))
    agent_input_sizes = p*np.array([1,1,1,1,1,1])
    neighbour_self_num = np.array([2,3,3,3,3,2])


    # data.edge_index = np.zeros((2, data.n_edges))

    # data.edge_index[:,0] =np.array([[0,0]])
    # data.edge_index[:,1] =np.array([[0,1]])
    # data.edge_index[:,2] =np.array([[0,2]])
    # data.edge_index[:,3] =np.array([[1,0]])
    # data.edge_index[:,4] =np.array([[2,0]])
    # data.edge_index[:,5] =np.array([[1,2]])
    # data.edge_index[:,6] =np.array([[2,1]])
    # data.edge_index[:,7] =np.array([[2,3]])
    # data.edge_index[:,8] =np.array([[3,2]])
    # data.edge_index[:,9] =np.array([[3,4]])
    # data.edge_index[:,10] =np.array([[4,3]])
    # data.edge_index[:,11] =np.array([[4,5]])
    # data.edge_index[:,12] =np.array([[5,4]])
    # data.edge_index[:,13] =np.array([[1,3]])
    # data.edge_index[:,14] =np.array([[3,1]])
    # data.edge_index[:,15] =np.array([[2,4]])
    # data.edge_index[:,16] =np.array([[4,2]])
    # data.edge_index[:,17] =np.array([[3,5]])
    # data.edge_index[:,18] =np.array([[5,3]])
    # data.edge_index[:,19] =np.array([[1,1]])
    # data.edge_index[:,20] =np.array([[2,2]])
    # data.edge_index[:,21] =np.array([[3,3]])
    # data.edge_index[:,22] =np.array([[4,4]])
    # data.edge_index[:,23] =np.array([[5,5]])

    # data.edge_index =  torch.from_numpy(data.edge_index).long()

    # edge_attr = np.zeros((data.n_edges, data.relation_dim))
    # edge_attr[0,0]= 1
    # edge_attr[1,1]= 1
    # edge_attr[2,2]= 1
    # edge_attr[3,3]= 1
    # edge_attr[4,4]= 1
    # edge_attr[5,5]= edge_attr[6,5]= edge_attr[7,5]= edge_attr[8,5]=  edge_attr[9,5] = edge_attr[10,5]= edge_attr[11,5]= edge_attr[12,5]= 1
    # edge_attr[13,6]= edge_attr[14,6]= edge_attr[15,6]= edge_attr[16,6]=  edge_attr[17,6] = edge_attr[18,6] = 1
    # edge_attr[19,7]= edge_attr[20,7]= edge_attr[21,7]= edge_attr[22,7]=  edge_attr[23,7] = 1

    

elif args.env == 'Soft':
    data.state_dim = 16
    data.n_nodes = 8
    data.n_edges = 64
    data.attr_dim = 4
    data.action_dim = 1
    data.relation_dim = 9 * 4

    edge_index = np.array([[],[]])
    for i in range(data.n_nodes):
        for j in range(data.n_nodes):
            #if rel_attrs[i,j].any():
            edge_index = np.append(edge_index, np.array([[i], [j]]), axis=1)
    
    data.edge_index = torch.from_numpy(edge_index).long()
    bs = 1 # due to flatening inside Koopman computation function
    Adj = np.array([[0,1,0,0,0,0,0,0],[1,0,0,1,0,0,0,0],[0,0,0,1,0,0,0,0],[0,1,1,0,1,0,0,1],[0,0,0,1,0,1,0,0],[0,0,0,0,1,0,1,0],[0,0,0,0,0,1,0,0],[0,0,0,1,0,0,0,0]])
    N=data.n_nodes
    m=args.g_dim
    p=data.action_dim = 1
    agent_sizes = np.ones((1,N))
    agent_input_sizes = p*np.array([1,1,1,1,1,1,1,1])
    neighbour_self_num = np.array([2,3,2,5,3,3,2,2])


elif args.env == 'Swim':
    data.state_dim = 16
    data.n_nodes = 8
    data.n_edges = 64
    data.attr_dim = 3
    data.action_dim = 1
    data.relation_dim = 9 * 3

    edge_index = np.array([[],[]])
    for i in range(data.n_nodes):
        for j in range(data.n_nodes):
            #if rel_attrs[i,j].any():
            edge_index = np.append(edge_index, np.array([[i], [j]]), axis=1)
    
    data.edge_index = torch.from_numpy(edge_index).long()
    bs = 1 # due to flatening inside Koopman computation function
    Adj = np.array([[0,1,0,0,0,0,0,0],[1,0,0,1,0,0,0,0],[0,0,0,1,0,0,0,0],[0,1,1,0,1,0,0,1],[0,0,0,1,0,1,0,0],[0,0,0,0,1,0,1,0],[0,0,0,0,0,1,0,0],[0,0,0,1,0,0,0,0]])
    N=data.n_nodes
    m=args.g_dim
    p=data.action_dim = 1
    agent_sizes = np.ones((1,N))
    agent_input_sizes = p*np.array([1,1,1,1,1,1,1,1])
    neighbour_self_num = np.array([2,3,2,5,3,3,2,2])

elif args.env == 'Grid':
    data.state_dim = 2
    data.n_nodes = 68
    data.n_edges = 86
    data.attr_dim = 3
    data.action_dim = 1
    data.relation_dim = 3
    

    with open('grid_prediction/Models/STGCN/dataset/PowerGrid/powerGrid_distance.csv', 'r') as f:
        reader = csv.reader(f)
        header = f.__next__()
        edges = [(int(i[0]), int(i[1])) for i in reader]

    data.edge_index =  torch.from_numpy(np.array(edges).T).long()


    # Adj = np.zeros((int(data.n_nodes), int(data.n_nodes)),
    #              dtype=np.float32)

    # for i, j in edges:
    #     Adj[i, j] = 1
    #     Adj[j, i] = 1

    
    bs=1
    N=5
    partition_type = args.partition_type 
    m=args.g_dim
    p=data.action_dim = 1
    
    neighbour_self_num = np.array([2,4,3,3,3])

    if partition_type == 1:
        Adj = np.array([[0 ,1 ,0 ,0 ,0],
        [1 ,0 ,1 ,0 ,1],
        [0, 1, 0, 1, 0],
        [0 ,0 ,1 ,0 ,1],
        [0, 1 ,0 ,1 ,0]])
        agent_sizes = np.array([[36,26,2,2,2]])
        agent_input_sizes = p*np.array([36,26,2,2,2])
        area_1_buses = np.sort(np.array([53 ,60, 25 ,2 ,26 ,27 ,28 ,29 ,61 ,24, 21 ,17, 3 ,18 ,16, 15, 14 ,4 ,19 ,22 ,58 ,56 ,11 ,12 ,10 ,5,8, 59 ,23 ,57 ,20 ,55 ,13 ,54, 6 ,7]))
        area_2_buses = np.sort([40 ,48 ,47, 1 ,31 ,62 ,63 ,32 ,38 ,46, 30 ,9 ,34 ,35, 49 ,36 ,45 ,51, 64 ,39 ,33 ,50 ,44, 43, 65 ,37])
        area_3_buses = np.array([41 ,66])
        area_4_buses = np.array([42 ,67])
        area_5_buses = np.array([52 ,68])

        all_buses = torch.from_numpy(np.concatenate([area_1_buses-1, area_2_buses-1, area_3_buses-1, area_4_buses-1, area_5_buses-1]))

        all_buses_reverse = np.zeros(68, dtype = np.int)

        for i in range(data.n_nodes):
            all_buses_reverse[i] = np.argwhere(all_buses == i)
        all_buses_reverse = torch.from_numpy(all_buses_reverse)

    elif partition_type == 2:
        Adj = np.array([[0 ,1 ,0 ,0 ,1],
        [1 ,0 ,1 ,1 ,1],
        [0, 1, 0, 0, 0],
        [0 ,1 ,0 ,0 ,1],
        [1, 1 ,0 ,1 ,0]])
        agent_sizes = np.array([[6,23,2,10,27]])
        agent_input_sizes = p*np.array([6,23,2,10,27])
        area_1_buses = np.sort(np.array([41, 42 ,52 ,66, 67, 68]))
        area_2_buses = np.sort([2, 3,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,40,54,57,58,60,61])
        area_3_buses = np.array([56 ,59])
        area_4_buses = np.array([4,5,6,7,8,10,11,12,13,14])
        area_5_buses = np.array([1,9,30,31,32,33,34,35,36,37,38,39,43,44,45,46,47,48,49,50,51,53,55,62,63,64,65])

        all_buses = torch.from_numpy(np.concatenate([area_1_buses-1, area_2_buses-1, area_3_buses-1, area_4_buses-1, area_5_buses-1]))

        all_buses_reverse = np.zeros(68, dtype = np.int)

        for i in range(data.n_nodes):
            all_buses_reverse[i] = np.argwhere(all_buses == i)
        all_buses_reverse = torch.from_numpy(all_buses_reverse)


elif args.env == 'Oscillator':
    data.state_dim = 2
    data.n_nodes = 50
    if args.nd == 'high':
        data.n_edges = 600
    elif args.nd == 'low':
        data.n_edges = 100
    elif args.nd == 'medium200':
        data.n_edges = 200
    elif args.nd == 'medium300':
        data.n_edges = 300
    elif args.nd == 'medium400':
        data.n_edges = 400
    elif args.nd == 'medium500':
        data.n_edges = 500
    
    
    data.attr_dim = 3
    data.action_dim = 1
    data.relation_dim = 6
    

    # with open('grid_prediction/Models/STGCN/dataset/PowerGrid/powerGrid_distance.csv', 'r') as f:
    #     reader = csv.reader(f)
    #     header = f.__next__()
    #     edges = [(int(i[0]), int(i[1])) for i in reader]

    if args.nd == 'high':
        edges = pd.read_csv('/qfs/projects/holonic_dmc/KGNN_data/data_oscillators/edge_info_high_nd_oscillator.csv',header=None).values
    elif args.nd == 'low':
        edges = pd.read_csv('/qfs/projects/holonic_dmc/KGNN_data/data_oscillators/edge_info_low_nd_oscillator.csv',header=None).values
    elif args.nd == 'medium200':
        edges = pd.read_csv('/qfs/projects/holonic_dmc/KGNN_data/data_oscillators/edge_info_200_oscillator.csv',header=None).values
    elif args.nd == 'medium300':
        edges = pd.read_csv('/qfs/projects/holonic_dmc/KGNN_data/data_oscillators/edge_info_300_oscillator.csv',header=None).values
    elif args.nd == 'medium400':
        edges = pd.read_csv('/qfs/projects/holonic_dmc/KGNN_data/data_oscillators/edge_info_400_oscillator.csv',header=None).values
    elif args.nd == 'medium500':
        edges = pd.read_csv('/qfs/projects/holonic_dmc/KGNN_data/data_oscillators/edge_info_500_oscillator.csv',header=None).values
    




    data.edge_index =  torch.from_numpy(np.array(edges).T).long()



    # Adj = np.zeros((int(data.n_nodes), int(data.n_nodes)),
    #              dtype=np.float32)

    # for i, j in edges:
    #     Adj[i, j] = 1
    #     Adj[j, i] = 1

    
    bs=1
    
    partition_type = args.partition_type 
    
    
    #neighbour_self_num = np.array([2,4,3,3,3])

    if partition_type == 1:
        N= data.n_nodes
        m=args.g_dim
        p=data.action_dim = 1
        if args.nd == 'high':
            properties = sio.loadmat('/qfs/projects/holonic_dmc/KGNN_data/data_oscillators/high_nd_system_parameters.mat')
        elif args.nd == 'low':
            properties = sio.loadmat('/qfs/projects/holonic_dmc/KGNN_data/data_oscillators/low_nd_system_parameters.mat')
        elif args.nd == 'medium200':
            properties = sio.loadmat('/qfs/projects/holonic_dmc/KGNN_data/data_oscillators/system_parameters_200.mat')
        elif args.nd == 'medium300':
            properties = sio.loadmat('/qfs/projects/holonic_dmc/KGNN_data/data_oscillators/system_parameters_300.mat')
        elif args.nd == 'medium400':
            properties = sio.loadmat('/qfs/projects/holonic_dmc/KGNN_data/data_oscillators/system_parameters_400.mat')
        elif args.nd == 'medium500':
            properties = sio.loadmat('/qfs/projects/holonic_dmc/KGNN_data/data_oscillators/system_parameters_500.mat')
        







        Adj = np.array(properties['Adj'])

        # Adj = np.array([[0 ,1 ,0 ,0 ,0],
        # [1 ,0 ,1 ,0 ,1],
        # [0, 1, 0, 1, 0],
        # [0 ,0 ,1 ,0 ,1],
        # [0, 1 ,0 ,1 ,0]])
        # agent_sizes = np.array([[36,26,2,2,2]])
        # agent_input_sizes = p*np.array([36,26,2,2,2])

        agent_sizes = np.ones((1,N))
        agent_input_sizes = p*np.ones((1,N))


        # area_1_buses = np.sort(np.array([53 ,60, 25 ,2 ,26 ,27 ,28 ,29 ,61 ,24, 21 ,17, 3 ,18 ,16, 15, 14 ,4 ,19 ,22 ,58 ,56 ,11 ,12 ,10 ,5,8, 59 ,23 ,57 ,20 ,55 ,13 ,54, 6 ,7]))
        # area_2_buses = np.sort([40 ,48 ,47, 1 ,31 ,62 ,63 ,32 ,38 ,46, 30 ,9 ,34 ,35, 49 ,36 ,45 ,51, 64 ,39 ,33 ,50 ,44, 43, 65 ,37])
        # area_3_buses = np.array([41 ,66])
        # area_4_buses = np.array([42 ,67])
        # area_5_buses = np.array([52 ,68])

        # all_buses = torch.from_numpy(np.concatenate([area_1_buses-1, area_2_buses-1, area_3_buses-1, area_4_buses-1, area_5_buses-1]))

        # all_buses_reverse = np.zeros(68, dtype = np.int)

        # for i in range(data.n_nodes):
        #     all_buses_reverse[i] = np.argwhere(all_buses == i)
        # all_buses_reverse = torch.from_numpy(all_buses_reverse)

    elif partition_type == 2:
        N= 5
        m=args.g_dim
        p=data.action_dim = 1
        if args.nd == 'high':
            #properties = sio.loadmat('/qfs/projects/holonic_dmc/KGNN_data/data_oscillators/high_nd_system_parameters.mat')
            Adj = np.array([[0,1,1 ,1,1],
            [1 ,0 ,1 ,1 ,1],
            [1, 1, 0, 1, 1],
            [1 ,1 ,1 ,0 ,1],
            [1, 1,1 ,1 ,0]])
            agent_sizes = np.array([[8,18,6,12,6]])
            agent_input_sizes = p*np.array([[8,18,6,12,6]])

            # agent_sizes = np.ones((1,N))
            # agent_input_sizes = p*np.ones((1,N))


            area_1_buses = np.sort(np.array([11, 12,13,14,25,31,36,37]))
            area_2_buses = np.sort([1,2,3,4,5,6,9,10,15,17,18,24,27,28,35,39,40,41])
            area_3_buses = np.array([19,20,22,32,45,48])
            area_4_buses = np.array([7,8,23,26,29,30,33,38,42,43,44,46])
            area_5_buses = np.array([16,21,34,47,49,50])

            all_buses = torch.from_numpy(np.concatenate([area_1_buses-1, area_2_buses-1, area_3_buses-1, area_4_buses-1, area_5_buses-1]))

            all_buses_reverse = np.zeros(data.n_nodes, dtype = np.int)

            for i in range(data.n_nodes):
                all_buses_reverse[i] = np.argwhere(all_buses == i)
            all_buses_reverse = torch.from_numpy(all_buses_reverse)

        elif args.nd == 'low':
            #properties = sio.loadmat('/qfs/projects/holonic_dmc/KGNN_data/data_oscillators/high_nd_system_parameters.mat')
            Adj = np.array([[0,1,1 ,1,1],
            [1 ,0 ,1 ,1 ,0],
            [1, 1, 0, 1, 1],
            [1 ,1 ,1 ,0 ,1],
            [1, 0,1 ,1 ,0]])
        #Adj = np.array(properties['Adj'])

            agent_sizes = np.array([[12,14,9,8,7]])
            agent_input_sizes = p*np.array([[12,14,9,8,7]])

            # agent_sizes = np.ones((1,N))
            # agent_input_sizes = p*np.ones((1,N))


            area_1_buses = np.sort(np.array([4,5,11,13,14,25,27,28,29,34,44,47]))
            area_2_buses = np.sort([1,3,12,15,23,26,31,35,37,40,43,45,46,50])
            area_3_buses = np.array([10,20,21,24,32,33,38,41,42])
            area_4_buses = np.array([2,8,9,18,19,22,30,36])
            area_5_buses = np.array([6,7,16,17,39,48,49])

            all_buses = torch.from_numpy(np.concatenate([area_1_buses-1, area_2_buses-1, area_3_buses-1, area_4_buses-1, area_5_buses-1]))

            all_buses_reverse = np.zeros(data.n_nodes, dtype = np.int)

            for i in range(data.n_nodes):
                all_buses_reverse[i] = np.argwhere(all_buses == i)
            all_buses_reverse = torch.from_numpy(all_buses_reverse)

        





if args.env == 'Oscillator':
    g_d = m*np.ones((1,N))
    #u_d = p*np.array([[36,26,2,2,2]])

    I = np.eye(N)

    ee = [[0] * N for i in range(N)]
    eu = [[0] * N for i in range(N)]

    Tfp_list = [[0] for i in range(N)]
    Rfp_list = [[0] for i in range(N)]
    Tpp_list = [[0] for i in range(N)]
    Rpp_list = [[0] for i in range(N)]
    Tup_list = [[0] for i in range(N)]
    Rup_list = [[0] for i in range(N)]
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    use_gpu = torch.cuda.is_available()
    for agent_k in range(N): 
        ep = I[:,agent_k]

        for k1 in range(N):

            for k2 in range(N):

                ee[k1][k2] = np.zeros((int(agent_sizes[0][k1]*g_d[0][k1]), int(agent_sizes[0][k2]*g_d[0][k2])))



        for k1 in range(N):
            ee[k1][k1] = np.kron(ep[k1],np.eye(int(agent_sizes[0][k1])*int(g_d[0][k1])))
        
        for k1 in range(N):
            for k2 in range(N):
                eu[k1][k2] = np.zeros((int(agent_input_sizes[0][k1]), int(agent_input_sizes[0][k2])))
        for k1 in range(N):
            eu[k1][k1] = np.kron(ep[k1],np.eye(int(agent_input_sizes[0][k1])))
        
        
        Tfp = np.block(ee)
        Rfp = Tfp[~np.all(Tfp == 0, axis=1)]
        # print(np.block(ee).shape)
        # print(Tfp)
        # print(Rfp)
        
        Tup= np.block(eu)
        Rup= Tup[~np.all(Tup == 0, axis=1)]
        
        ap = Adj[:,agent_k].reshape(N,1)
        # print(ap)
        ep = np.zeros((N,1))
        # print(ep)
        ep[agent_k] =1
        
        aep = ap+ep;
        # print(aep)
        ae = [[0] * N for i in range(N)]
                
        for k1 in range(N):

            for k2 in range(N):

                ae[k1][k2] = np.zeros((int(agent_sizes[0][k1]*g_d[0][k1]), int(agent_sizes[0][k2]*g_d[0][k2])))

                
        for k1 in range(N):
            ae[k1][k1] = np.kron(aep[k1],np.eye(int(agent_sizes[0][k1])*int(g_d[0][k1])))

        Tpp = np.block(ae)
        Rpp = Tpp[~np.all(Tpp == 0, axis=1)]
        # print(Tpp)
        # print(Rpp)
        

        Tfp_list[agent_k] = torch.from_numpy(Tfp[None,:,:].repeat(bs, axis=0)).float().to(device)
        Rfp_list[agent_k] = torch.from_numpy(Rfp[None,:,:].repeat(bs, axis=0)).float().to(device)
        Tpp_list[agent_k] = torch.from_numpy(Tpp[None,:,:].repeat(bs, axis=0)).float().to(device)
        Rpp_list[agent_k] = torch.from_numpy(Rpp[None,:,:].repeat(bs, axis=0)).float().to(device)
        Tup_list[agent_k] = torch.from_numpy(Tup[None,:,:].repeat(bs, axis=0)).float().to(device)
        Rup_list[agent_k] = torch.from_numpy(Rup[None,:,:].repeat(bs, axis=0)).float().to(device)

else:
    g_d = m*np.ones((1,N))
    u_d = p*np.ones((1,N))

    I = np.eye(N)

    ee = [[0] * N for i in range(N)]
    eu = [[0] * N for i in range(N)]

    Tfp_list = [[0] for i in range(N)]
    Rfp_list = [[0] for i in range(N)]
    Tpp_list = [[0] for i in range(N)]
    Rpp_list = [[0] for i in range(N)]
    Tup_list = [[0] for i in range(N)]
    Rup_list = [[0] for i in range(N)]
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    use_gpu = torch.cuda.is_available()
    for agent_k in range(N): 
        ep = I[:,agent_k]

        for k1 in range(N):

            for k2 in range(N):

                ee[k1][k2] = np.zeros((int(agent_sizes[0][k1]*g_d[0][k1]), int(agent_sizes[0][k2]*g_d[0][k2])))



        for k1 in range(N):
            ee[k1][k1] = np.kron(ep[k1],np.eye(int(agent_sizes[0][k1])*int(g_d[0][k1])))
        
        for k1 in range(N):
            for k2 in range(N):
                eu[k1][k2] = np.zeros((int(agent_input_sizes[k1]), int(agent_input_sizes[k2])))
        for k1 in range(N):
            eu[k1][k1] = np.kron(ep[k1],np.eye(int(agent_input_sizes[k1])))
        
        
        Tfp = np.block(ee)
        Rfp = Tfp[~np.all(Tfp == 0, axis=1)]
        # print(np.block(ee).shape)
        # print(Tfp)
        # print(Rfp)
        
        Tup= np.block(eu)
        Rup= Tup[~np.all(Tup == 0, axis=1)]
        
        ap = Adj[:,agent_k].reshape(N,1)
        # print(ap)
        ep = np.zeros((N,1))
        # print(ep)
        ep[agent_k] =1
        
        aep = ap+ep;
        # print(aep)
        ae = [[0] * N for i in range(N)]
                
        for k1 in range(N):

            for k2 in range(N):

                ae[k1][k2] = np.zeros((int(agent_sizes[0][k1]*g_d[0][k1]), int(agent_sizes[0][k2]*g_d[0][k2])))

                
        for k1 in range(N):
            ae[k1][k1] = np.kron(aep[k1],np.eye(int(agent_sizes[0][k1])*int(g_d[0][k1])))

        Tpp = np.block(ae)
        Rpp = Tpp[~np.all(Tpp == 0, axis=1)]
        # print(Tpp)
        # print(Rpp)
        

        Tfp_list[agent_k] = torch.from_numpy(Tfp[None,:,:].repeat(bs, axis=0)).float().to(device)
        Rfp_list[agent_k] = torch.from_numpy(Rfp[None,:,:].repeat(bs, axis=0)).float().to(device)
        Tpp_list[agent_k] = torch.from_numpy(Tpp[None,:,:].repeat(bs, axis=0)).float().to(device)
        Rpp_list[agent_k] = torch.from_numpy(Rpp[None,:,:].repeat(bs, axis=0)).float().to(device)
        Tup_list[agent_k] = torch.from_numpy(Tup[None,:,:].repeat(bs, axis=0)).float().to(device)
        Rup_list[agent_k] = torch.from_numpy(Rup[None,:,:].repeat(bs, axis=0)).float().to(device)



# finish comment




    # if use_gpu:
    #     Tfp_list = Tfp_list.to(device)
    #     Rfp_list = Rfp_list.to(device)
    #     Tpp_list = Tpp_list.to(device)
    #     Rpp_list = Rpp_list.to(device)
    #     Tup_list = Tup_list.to(device)
    #     Rup_list = Rup_list.to(device)

    

    

# data.x_cat = torch.cat([data.states, data.attr], -1)



class args_e:
    pass 
class args2_e:
    pass 
class args_d:
    pass 
class args2_d:
    pass 
args_e.state_dim, args_e.attr_dim, args_e.action_dim, args_e.relation_dim = data.state_dim, data.attr_dim, data.action_dim, data.relation_dim
args_e.nf_particle, args_e.nf_relation, args_e.nf_effect = 70, 70, 70  #100
args_e.n_edges = data.n_edges
args_e.output_dim = args_e.nf_particle

# define arguments for the second layer
g_dim = args.g_dim  #32
args2_e.state_dim, args2_e.attr_dim, args2_e.action_dim, args2_e.relation_dim = args_e.nf_particle, data.attr_dim, data.action_dim, data.relation_dim
args2_e.nf_particle, args2_e.nf_relation, args2_e.nf_effect = g_dim, 70, 70
args2_e.n_edges = data.n_edges
args2_e.output_dim = args2_e.nf_particle

# decoder
args_d.state_dim, args_d.attr_dim, args_d.action_dim, args_d.relation_dim = args2_e.output_dim, data.attr_dim, data.action_dim, data.relation_dim
args_d.nf_particle, args_d.nf_relation, args_d.nf_effect = args_e.state_dim, 70, 70
args_d.n_edges = data.n_edges
args_d.output_dim = args_d.nf_particle

# define arguments for the second layer of decoder
args2_d.state_dim, args2_d.attr_dim, args2_d.action_dim, args2_d.relation_dim = args_d.nf_particle, data.attr_dim, data.action_dim, data.relation_dim
args2_d.nf_particle, args2_d.nf_relation, args2_d.nf_effect = args_e.state_dim, 70, 70
args2_d.n_edges = data.n_edges
args2_d.output_dim = args_d.nf_particle
