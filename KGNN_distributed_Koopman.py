from abc import ABC
from config_kgnn import gen_args
import numpy as np
import torch
import torch.nn as nn
from torch.autograd import Variable
import torch.optim as optim
from torch.optim.lr_scheduler import ReduceLROnPlateau
import torch.nn as nn
from typing import Callable, Tuple, Union
from torch_geometric.typing import OptPairTensor, Adj, OptTensor, Size

from torch import Tensor
import torch.nn.functional as F
from torch_sparse import SparseTensor, matmul
from torch_geometric.nn.conv import MessagePassing
from torch_geometric.datasets import Planetoid
#from data import denormalize, normalize
#from utils import load_data
from torch_geometric.nn.conv import MessagePassing
from utils import count_parameters, Tee, AverageMeter, rand_int, mix_iters, get_flat, print_args

args_in = gen_args()

class RelationEncoder(nn.Module):
    def __init__(self, input_size, hidden_size, output_size):
        super(RelationEncoder, self).__init__()

        self.model = nn.Sequential(
            nn.Linear(input_size, hidden_size),
            nn.ReLU(),
            nn.Linear(hidden_size, output_size),
            nn.ReLU()
        )

    def forward(self, x):
        """
        Args:
            x: [n_relations/n_edges in PyTorch Geometric, input_size]
        Returns:
            [n_relations/n_edges in PyTorch Geometric, output_size]
        """
        return self.model(x)

class ParticleEncoder(nn.Module):
    def __init__(self, input_size, hidden_size, output_size):
        super(ParticleEncoder, self).__init__()

        self.model = nn.Sequential(
            nn.Linear(input_size, hidden_size),
            nn.ReLU(),
            nn.Linear(hidden_size, output_size),
            nn.ReLU()
        )

    def forward(self, x):
        """
        Args:
            x: [n_particles/n_edges in PyTorch Geometric, input_size]
        Returns:
            [n_particles/n_edges in PyTorch Geometric, output_size]
        """
        return self.model(x)


class Propagator(nn.Module):
    def __init__(self, input_size, output_size, residual=False):
        super(Propagator, self).__init__()

        self.residual = residual

        self.linear = nn.Linear(input_size, output_size)
        self.relu = nn.ReLU()

    def forward(self, x, res=None):
        """
        Args:
            x: [n_relations/n_particles, input_size]
        Returns:
            [n_relations/n_particles, output_size]
        """
        if self.residual:
            x = self.relu(self.linear(x) + res)
        else:
            x = self.relu(self.linear(x))

        return x


# TODO: the predictor is not implemented yet

class ParticlePredictor(nn.Module):
    def __init__(self, input_size, hidden_size, output_size):
        super(ParticlePredictor, self).__init__()

        self.linear_0 = nn.Linear(input_size, hidden_size)
        self.linear_1 = nn.Linear(hidden_size, output_size)
        self.relu = nn.ReLU()

    def forward(self, x):
        """
        Args:
            x: [n_particles, input_size]
        Returns:
            [n_particles, output_size]
        """
        x = self.relu(self.linear_0(x))

        return self.linear_1(x)



class PropagationNetwork(MessagePassing):
    def __init__(self, args=None, aggr: str = 'add', input_particle_dim=None, input_relation_dim=None, output_dim=None, action=True, tanh=False,
                 residual=False, use_gpu=False):

        super(PropagationNetwork, self).__init__(aggr=aggr)
        self.args = args
        self.action = action
        self.state_dim = args.state_dim
        self.attr_dim = args.attr_dim
        self.action_dim = args.action_dim
        self.relation_dim = args.relation_dim
        self.n_edges = args.n_edges

        self.output_dim = args.output_dim
        #if input_particle_dim is None:
        input_particle_dim = self.attr_dim + self.state_dim
            #input_particle_dim += self.action_dim if action else 0

        #if input_relation_dim is None:
        input_relation_dim = self.state_dim + self.relation_dim + self.attr_dim * 2

        nf_particle = args.nf_particle
        nf_relation = args.nf_relation
        nf_effect = args.nf_effect

        self.nf_effect = args.nf_effect

        self.use_gpu = use_gpu
        self.residual = residual

        # (1) state
        self.obj_encoder = ParticleEncoder(input_particle_dim, nf_particle, nf_effect)

        # (1) state receiver (2) state_diff
        self.relation_encoder = RelationEncoder(input_relation_dim, nf_relation, nf_relation)

        # (1) relation encode (2) sender effect (3) receiver effect
        self.relation_propagator = Propagator(nf_relation + 2 * nf_effect, nf_effect)

        # (1) particle encode (2) particle effect
        self.particle_propagator = Propagator(2 * nf_effect, nf_effect, self.residual)

        # rigid predictor
        # (1) particle encode (2) set particle effect

        self.particle_predictor = ParticlePredictor(nf_effect, nf_effect, self.output_dim)

        # if tanh:
        #     self.particle_predictor = nn.Sequential(
        #         self.particle_predictor, nn.Tanh()
        #     )

    def forward(self, x: Union[Tensor, OptPairTensor], edge_index, edge_attr):
        # x has shape [N, in_channels]
        # edge_index has shape [2, E]
        """

        x concatenates states, attrs, actions
        rel_attrs plays the role of edge_attr
        x shape will be then [B x N x (state_dim + attr_dim + action_dim)]
        edge_attr : [B x E x relation_dim]

        :return:
        """
        self.N = x.size(1) # check command
        
        # these are not used yet directly
        states = x[:,:,0:self.state_dim]
        attrs = x[:,:, self.state_dim: self.state_dim+self.attr_dim]
        #actions = x[:,:,self.state_dim+self.attr_dim: self.state_dim+ self.attr_dim+ self.action_dim]

        if isinstance(x, Tensor):
            x: OptPairTensor = (x, x)

        # call propagate
        out = self.propagate(edge_index, x=x, edge_attr=edge_attr)

        x_r = x[1]
        if x_r is not None:
            state_r = x_r[:,:,0:self.state_dim]
            attr_r = x_r[:,:, self.state_dim: self.state_dim+self.attr_dim]
            #action_r = x_r[:,:,self.state_dim+self.attr_dim: self.state_dim+ self.attr_dim+ self.action_dim]

            obj_input_list = [attr_r, state_r]
            # if self.action:
            #     obj_input_list += [action_r]

            tmp = torch.cat(obj_input_list, 2)
            obj_encode = self.obj_encoder(tmp)

            tmp1 = torch.cat([obj_encode, out], 2)
            obj_encode = self.particle_propagator(tmp1)
        

        # How to implement particle predictor? 
        # if last layer:
        obj_prediction = self.particle_predictor(obj_encode)
        # else:
        #     obj_prediction = obj_encode
        #obj_prediction = obj_encode
        return obj_prediction
        

    def message(self, x_i, x_j, edge_index, edge_attr):

        states_i = x_i[:,:,0:self.state_dim] 
        states_j = x_j[:,:,0:self.state_dim] 
        attrs_i = x_i[:,:, self.state_dim: self.state_dim+self.attr_dim]
        attrs_j = x_j[:,:, self.state_dim: self.state_dim+self.attr_dim]
        # actions_i = x_i[:,:,self.state_dim+self.attr_dim: self.state_dim+ self.attr_dim+ self.action_dim]
        # actions_j = x_j[:,:,self.state_dim+self.attr_dim: self.state_dim+ self.attr_dim+ self.action_dim]
        

        '''encode node i'''
        obj_input_list_i = [attrs_i, states_i]
        # if self.action:
        #     obj_input_list_i += [actions_i]

        tmp = torch.cat(obj_input_list_i, 2)
        obj_encode_i = self.obj_encoder(tmp.reshape(tmp.size(0)*tmp.size(1),-1)).reshape(x_i.shape[0], self.n_edges, -1)
        

        '''encode node j'''
        obj_input_list_j = [attrs_j, states_j]
        # if self.action:
        #     obj_input_list_j += [actions_j]

        tmp = torch.cat(obj_input_list_j, 2)
        obj_encode_j = self.obj_encoder(tmp.reshape(tmp.size(0)*tmp.size(1),-1)).reshape(x_i.shape[0], self.n_edges, -1)
        
        '''encode edge'''
        # relative states betwenn i and j
        rel_states = states_i - states_j
        
        receiver_attr = attrs_i
        sender_attr = attrs_j

        # concatenate rel_states, rel_attr, sender and receiver node attrs
        tmp = torch.cat([edge_attr, rel_states, receiver_attr, sender_attr], 2)
        rel_encode = self.relation_encoder(tmp)


        receiver_code = obj_encode_i
        sender_code = obj_encode_j
        tmp1 = torch.cat([rel_encode, receiver_code, sender_code], 2)

        rel_effect = self.relation_propagator(tmp1)

        return rel_effect







# modified with batch size
class GraphNet(torch.nn.Module):
    def __init__(self, args, args2, action=False, tanh=True, 
            residual=False, use_gpu=False):
        super(GraphNet, self).__init__()
        
        self.gnn1 = PropagationNetwork(args=args, action=action, tanh=tanh,  # use tanh to enforce the shape of the code space
            residual=residual, use_gpu=use_gpu)
        self.gnn2 = PropagationNetwork(args=args2, action=action, tanh=tanh,  # use tanh to enforce the shape of the code space
            residual=residual, use_gpu=use_gpu)
        

    def forward(self, data):
        x, edge_index, edge_attr = data.x_cat, data.edge_index, data.edge_attr

        x = self.gnn1(x, edge_index, edge_attr)
        x = F.relu(x)
        data.x_cat2 = torch.cat([x, data.attr], 2)
        x = data.x_cat2
        x = self.gnn2(x, edge_index, edge_attr)
        

        return x


class GNNKoopman(nn.Module, ABC):
    def __init__(self, args_e, args2_e, args_d, args2_d, action = True, tanh = True, residual=False, use_gpu=False):
        super(GNNKoopman, self).__init__()

        self.args_e = args_e
        self.args2_e = args2_e
        self.args_d = args_d
        self.args2_d = args2_d


        #g_dim = args2_e.output_dim

        #self.nf_effect = args.nf_effect

        self.use_gpu = use_gpu
        self.residual = residual

        ''' state '''
        # we should not include action in state encoder
        # input_particle_dim = args.attr_dim + args.state_dim
        # input_relation_dim = args.state_dim + args.relation_dim + args.attr_dim * 2

        # print('state_encoder', 'node', input_particle_dim, 'edge', input_relation_dim)

        self.state_encoder = GraphNet(
            self.args_e, self.args2_e, action=action, tanh=tanh,  # use tanh to enforce the shape of the code space
            residual=residual, use_gpu=use_gpu)

        # the state for decoding phase is replaced with code of g_dim
        # input_particle_dim = args.attr_dim + args.g_dim
        # input_relation_dim = args.g_dim + args.relation_dim + args.attr_dim * 2

        # print('state_decoder', 'node', input_particle_dim, 'edge', input_relation_dim)


        self.state_decoder = GraphNet(
            self.args_d,  self.args2_d, action=action, tanh=tanh,  # use tanh to enforce the shape of the code space
            residual=residual, use_gpu=use_gpu)

        ''' dynamical system matrices: A and B '''
        self.A = None
        self.B = None
        
        self.system_identify = self.fit_unstructured
        self.system_identify_distributed = self.fit_unstructured_distributed
        self.simulate = self.rollout_unstructured
        self.step = self.linear_forward_unstructured
        
    def to_g(self, data_states):
        """ state encoder """

        return self.state_encoder(data_states)

    def to_s(self, data_g):
        """ state decoder """

        # if self.args.env in ['Soft', 'Swim']:
        #     states = self.state_decoder(attrs=attrs, states=gcodes, actions=None, rel_attrs=rel_attrs, pstep=pstep)
        #     return regularize_state_Soft(states, rel_attrs, self.stat)

        return self.state_decoder(data_g)

    
    # issue with rel_attrs (NxNxR) and edge_attr(ExR)
    # we can use E = NxN and then reshape
    # @staticmethod
    # def get_aug(G, rel_attrs):
    #     """
    #     :param G: B x T x N x D
    #     :param rel_attrs:  B x N x N x R
    #     :return:
    #     """
    #     B, T, N, D = G.size()
    #     R = rel_attrs.size(-1)

    #     sumG_list = []
    #     for i in range(R):
    #         ''' B x T x N x N '''
    #         adj = rel_attrs[:, :, :, i][:, None, :, :].repeat(1, T, 1, 1)
    #         sumG = torch.bmm(
    #             adj.reshape(B * T, N, N),
    #             G.reshape(B * T, N, D)
    #         ).reshape(B, T, N, D)
    #         sumG_list.append(sumG)

    #     augG = torch.cat(sumG_list, 3)

    #     return augG

    
    # unstructured large A

    def fit_unstructured(self, G, H, U, I_factor, rel_attrs=None):
        """
        :param G: B x T x N x D    basically batch size here is 1 as flatten data ia passed
        :param H: B x T x N x D
        :param U: B x T x N x a_dim
        :param I_factor: scalor
        :return: A, B
        s.t.
        H = catG @ A + catU @ B
        """
        bs, T, N, D = G.size()
        G = G.reshape(bs, T, -1)
        H = H.reshape(bs, T, -1)
        U = U.reshape(bs, T, -1)

        G_U = torch.cat([G, U], 2)
        A_B = torch.bmm(
            self.batch_pinv(G_U, I_factor),
            H
        )
        self.A = A_B[:, :N * D]
        self.B = A_B[:, N * D:]

        fit_err = H - torch.bmm(G_U, A_B)
        fit_err = torch.sqrt((fit_err ** 2).mean())

        return self.A, self.B, fit_err



    # def fit_unstructured_distributed(self, G, H, U, I_factor, rel_attrs=None):
    #     """
    #     :param G: B x T x N x D    basically batch size here is 1 as flatten data ia passed
    #     :param H: B x T x N x D
    #     :param U: B x T x N x a_dim
    #     :param I_factor: scalor
    #     :return: A, B
    #     s.t.
    #     H = catG @ A + catU @ B
    #     """
    #     N=G.shape[-2] #6
    #     Adj = np.array([[0,1,0,0,0,0],[1,0,1,0,0,0],[0,1,0,1,0,0],[0,0,1,0,1,0],[0,0,0,1,0,1],[0,0,0,0,1,0]])

    #     #print(Adj)
        
    #     m=G.shape[-1] #32
    #     Degree = np.zeros((N,N))
    #     Degree[0,0]=1
    #     Degree[1,1]=2
    #     Degree[2,2]=2
    #     Degree[3,3]=2
    #     Degree[4,4]=2
    #     Degree[5,5]=1
    
    #     p=U.shape[-1] #1
    #     agent_sizes = np.ones((1,N))
    #     agent_input_sizes = p*np.array([1,1,1,1,1,1])
    #     neighbour_self_num = np.array([2,3,3,3,3,2])

    #     g_d = m*np.ones((1,N))
    #     u_d = p*np.ones((1,N))

    #     I = np.eye(N)
    #     ee = [[0] * N for i in range(N)]
    #     eu = [[0] * N for i in range(N)]
        
    #     bs, T, N, D = G.size()
    #     z = [[0] for i in range(bs)]

    #     device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    #     use_gpu = torch.cuda.is_available()
    #     # if use_gpu:
    #     #     Adj = Adj.to(device)
    #     #     agent_sizes = agent_sizes.to(device)
    #     #     agent_input_sizes = agent_input_sizes.to(device)
    #     #     neighbour_self_num = neighbour_self_num.to(device)
    #     #     g_d = g_d.to(device)
    #     #     u_d = u_d.to(device)
    #     #     I = I.to(device)
    #     #     ee = ee.to(device)
    #     #     eu = eu.to(device)
    #     #     z = z.to(device)



    #     G = G.reshape(bs, T, -1)
    #     H = H.reshape(bs, T, -1)
    #     U = U.reshape(bs, T, -1)
        
    #     for agent_k in range(N): 
    #         ep = I[:,agent_k]

    #         for k1 in range(N):

    #             for k2 in range(N):

    #                 ee[k1][k2] = np.zeros((int(agent_sizes[0][k1]*g_d[0][k1]), int(agent_sizes[0][k2]*g_d[0][k2])))



    #         for k1 in range(N):
    #             ee[k1][k1] = np.kron(ep[k1],np.eye(int(agent_sizes[0][k1])*int(g_d[0][k1])))

            
            
    #         for k1 in range(N):
    #             for k2 in range(N):
    #                 eu[k1][k2] = np.zeros((int(agent_input_sizes[k1]), int(agent_input_sizes[k2])))
    #         for k1 in range(N):
    #             eu[k1][k1] = np.kron(ep[k1],np.eye(int(agent_input_sizes[k1])))
    
            
            
            
            
    #         Tfp = np.block(ee)
    #         Rfp = Tfp[~np.all(Tfp == 0, axis=1)]
    #         print(np.block(ee).shape)
    #         #print(Tfp)
    #         #print(Rfp)
            

    #         Tup= np.block(eu)
    #         Rup= Tup[~np.all(Tup == 0, axis=1)]
            
    #         ap = Adj[:,agent_k].reshape(N,1)
    #         print(ap)
    #         ep = np.zeros((N,1))
    #         print(ep)
    #         ep[agent_k] =1
            
    #         aep = ap+ep;
    #         print(aep)
    #         ae = [[0] * N for i in range(N)]
                    
    #         for k1 in range(N):

    #             for k2 in range(N):

    #                 ae[k1][k2] = np.zeros((int(agent_sizes[0][k1]*g_d[0][k1]), int(agent_sizes[0][k2]*g_d[0][k2])))

                    
    #         for k1 in range(N):
    #             ae[k1][k1] = np.kron(aep[k1],np.eye(int(agent_sizes[0][k1])*int(g_d[0][k1])))

    #         Tpp = np.block(ae)
    #         Rpp = Tpp[~np.all(Tpp == 0, axis=1)]
    #         #print(Tpp)
    #         #print(Rpp)



    #         Tfp = torch.from_numpy(Tfp[None,:,:].repeat(bs, axis=0)).float()
    #         Rfp = torch.from_numpy(Rfp[None,:,:].repeat(bs, axis=0)).float()
    #         Tpp = torch.from_numpy(Tpp[None,:,:].repeat(bs, axis=0)).float()
    #         Rpp = torch.from_numpy(Rpp[None,:,:].repeat(bs, axis=0)).float()
    #         Tup = torch.from_numpy(Tup[None,:,:].repeat(bs, axis=0)).float()
    #         Rup = torch.from_numpy(Rup[None,:,:].repeat(bs, axis=0)).float()


    #         if use_gpu:
    #             Tfp = Tfp.to(device)
    #             Rfp = Rfp.to(device)
    #             Tpp = Tpp.to(device)
    #             Rpp = Rpp.to(device)
    #             Tup = Tup.to(device)
    #             Rup = Rup.to(device)


    #         G_agent = torch.matmul(torch.matmul(Rpp,Tpp), torch.transpose(G,1,2))
    #         G_agent = torch.transpose(G_agent,1,2)

    #         H_agent = torch.matmul(torch.matmul(Rfp,Tfp), torch.transpose(H,1,2))
    #         H_agent = torch.transpose(H_agent,1,2)

    #         U_agent = torch.matmul(torch.matmul(Rup,Tup), torch.transpose(U,1,2))
    #         U_agent = torch.transpose(U_agent,1,2)



    #         G_U_agent = torch.cat([G_agent, U_agent], 2)
    #         A_B_agent = torch.bmm(
    #         self.batch_pinv(G_U_agent, I_factor),
    #         H_agent
    #         )
    #         A_agent = torch.bmm(torch.transpose(Rpp,1,2), A_B_agent[:,:int(neighbour_self_num[agent_k]*g_d[0][agent_k])])
    #         B_agent = A_B_agent[:,int(neighbour_self_num[agent_k]*g_d[0][agent_k]):]
    #         if agent_k==0:
    #             A=A_agent
    #         else:
    #             A=torch.cat([A, A_agent],2)
            
    #         if agent_k==0:
    #             B = B_agent
            
    #         else:
    #             for k in range(bs):
    #                 z[k] = torch.block_diag(B[k,:,:], B_agent[k,:,:])
    #             B=torch.stack(z)

    #     G_U = torch.cat([G, U], 2)
    #     A_B = torch.cat([A, B], 1)
    #     fit_err = H - torch.bmm(G_U, A_B)
    #     fit_err = torch.sqrt((fit_err ** 2).mean())
    #     self.A = A
    #     self.B = B

    #     return self.A, self.B, fit_err


    def fit_unstructured_distributed(self, G, H, U, I_factor, Adj, agent_sizes, g_d, Tfp_list, Rfp_list, Tpp_list, Rpp_list, Tup_list, Rup_list, rel_attrs=None):
        """
        :param G: B x T x N x D    basically batch size here is 1 as flatten data ia passed
        :param H: B x T x N x D
        :param U: B x T x N x a_dim
        :param I_factor: scalor
        :return: A, B
        s.t.
        H = catG @ A + catU @ B
        """
    
        

        #print(Adj)
        
        m=G.shape[-1] #32
        
    
        p=U.shape[-1] #1
        
        

        

        
        
        bs, T, N1, D = G.size()
        if args_in.env == 'Oscillator':
            if args_in.partition_type == 1:
                N=N1
            else:
                N=5
        elif args_in.env == 'Grid':
            N=5
        elif args_in.env == 'Rope':
            N=N1    
        
        z = [[0] for i in range(bs)]

        device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        use_gpu = torch.cuda.is_available()
        # if use_gpu:
        #     Adj = Adj.to(device)
        #     agent_sizes = agent_sizes.to(device)
        #     agent_input_sizes = agent_input_sizes.to(device)
        #     neighbour_self_num = neighbour_self_num.to(device)
        #     g_d = g_d.to(device)
        #     u_d = u_d.to(device)
        #     I = I.to(device)
        #     ee = ee.to(device)
        #     eu = eu.to(device)
        #     z = z.to(device)



        G = G.reshape(bs, T, -1)
        H = H.reshape(bs, T, -1)
        U = U.reshape(bs, T, -1)
        
        for agent_k in range(N): 
            

            G_agent = torch.matmul(torch.matmul(Rpp_list[agent_k],Tpp_list[agent_k]), torch.transpose(G,1,2))
            G_agent = torch.transpose(G_agent,1,2)

            H_agent = torch.matmul(torch.matmul(Rfp_list[agent_k],Tfp_list[agent_k]), torch.transpose(H,1,2))
            H_agent = torch.transpose(H_agent,1,2)

            U_agent = torch.matmul(torch.matmul(Rup_list[agent_k],Tup_list[agent_k]), torch.transpose(U,1,2))
            U_agent = torch.transpose(U_agent,1,2)



            G_U_agent = torch.cat([G_agent, U_agent], 2)
            A_B_agent = torch.bmm(
            self.batch_pinv(G_U_agent, I_factor),
            H_agent
            )
            A_agent = torch.bmm(torch.transpose(Rpp_list[agent_k],1,2), A_B_agent[:,:int((agent_sizes[0][agent_k]+np.dot(Adj[agent_k],agent_sizes.T))*g_d[0][agent_k])])
            B_agent = A_B_agent[:,int((agent_sizes[0][agent_k]+np.dot(Adj[agent_k],agent_sizes.T))*g_d[0][agent_k]):]
            if agent_k==0:
                A=A_agent
            else:
                A=torch.cat([A, A_agent],2)
            
            if agent_k==0:
                B = B_agent
            
            else:
                for k in range(bs):
                    z[k] = torch.block_diag(B[k,:,:], B_agent[k,:,:])
                B=torch.stack(z)

        G_U = torch.cat([G, U], 2)
        A_B = torch.cat([A, B], 1)
        fit_err = H - torch.bmm(G_U, A_B)
        fit_err = torch.sqrt((fit_err ** 2).mean())
        self.A = A
        self.B = B

        return self.A, self.B, fit_err
            






    def linear_forward_unstructured(self, g, u, rel_attrs=None):
        B, N, D = g.size()
        a_dim = u.size(-1)
        g = g.reshape(B, 1, N * D)
        u = u.reshape(B, 1, N * a_dim)
        new_g = torch.bmm(g, self.A) + torch.bmm(u, self.B)
        return new_g.reshape(B, N, D)

    def rollout_unstructured(self, g, u_seq, T, rel_attrs=None):
        g_list = []
        for t in range(T):
            g = self.linear_forward_unstructured(g, u_seq[:, t])
            g_list.append(g[:, None, :, :])
        return torch.cat(g_list, 1)
    
    @staticmethod
    def batch_pinv(x, I_factor):

        """
        :param x: B x N x D (N > D)
        :param I_factor:
        :return:
        """

        B, N, D = x.size()

        if N < D:
            x = torch.transpose(x, 1, 2)
            N, D = D, N
            trans = True
        else:
            trans = False

        x_t = torch.transpose(x, 1, 2)

        use_gpu = torch.cuda.is_available()
        I = torch.eye(D)[None, :, :].repeat(B, 1, 1)
        if use_gpu:
            I = I.cuda()

        x_pinv = torch.bmm(
            torch.inverse(torch.bmm(x_t, x) + I_factor * I),
            x_t
        )

        if trans:
            x_pinv = torch.transpose(x_pinv, 1, 2)

        return x_pinv