import os

from config_kgnn import gen_args
from data import normalize, denormalize
from models.CompositionalKoopmanOperators import CompositionalKoopmanOperators
from models.KoopmanBaselineModel import KoopmanBaseline
from physics_engine import SoftEngine, RopeEngine, SwimEngine
from utils import *
from utils import to_var, to_np, Tee
from progressbar import ProgressBar
from KGNN_distributed_Koopman import *
import time
import random
import torch
import torch.nn as nn
from torch.autograd import Variable
import torch.optim as optim
from torch.optim.lr_scheduler import ReduceLROnPlateau
import torch.nn as nn
from progressbar import ProgressBar
from typing import Callable, Union
from torch_geometric import datasets
from torch_geometric.typing import OptPairTensor, Adj, OptTensor, Size
from matplotlib import pyplot as plt
from data_inputs_kgnn import data, args_e, args2_e, args_d, args2_d # important, added this
from data_inputs_kgnn import neighbour_self_num, g_d, Tfp_list, Rfp_list, Tpp_list, Rpp_list, Tup_list, Rup_list 
from scipy.io import savemat

args = gen_args()
print_args(args)
'''
args.fit_num is # of trajectories used for SysID
'''
assert args.group_size - 1 >= args.fit_num

data_names = ['attrs', 'states', 'actions']
prepared_names = ['attrs', 'states', 'actions', 'rel_attrs']

data_dir = os.path.join(args.dataf, args.eval_set)

print(f"Load stored dataset statistics from {args.stat_path}!")
stat = load_data(data_names, args.stat_path)

# if args.env == 'Rope':
#     engine = RopeEngine(args.dt, args.state_dim, args.action_dim, args.param_dim)
# elif args.env == 'Soft':
#     engine = SoftEngine(args.dt, args.state_dim, args.action_dim, args.param_dim)
# elif args.env == 'Swim':
#     engine = SwimEngine(args.dt, args.state_dim, args.action_dim, args.param_dim)
# else:
#     assert False


os.system('mkdir -p ' + args.evalf)
log_path = os.path.join(args.evalf, 'log.txt')
tee = Tee(log_path, 'w')




# state shape -- torch.Size([8, 65, 5, 4])


# data.state_dim = 4
# data.n_nodes = 6
# data.n_edges = 24
# data.attr_dim = 2
# data.action_dim = 1
# data.relation_dim = 8
# # data.states = torch.rand(bs, T, data.n_nodes,data.state_dim)
# # data.attr = torch.rand(bs, T, data.n_nodes,data.attr_dim)
# # data.action = torch.rand(bs, T, data.n_nodes,data.action_dim)
# # data.edge_attr = torch.rand(bs, T, data.n_edges,data.relation_dim)
# data.edge_index = np.zeros((2, data.n_edges))

# data.edge_index[:,0] =np.array([[0,0]])
# data.edge_index[:,1] =np.array([[0,1]])
# data.edge_index[:,2] =np.array([[0,2]])
# data.edge_index[:,3] =np.array([[1,0]])
# data.edge_index[:,4] =np.array([[2,0]])
# data.edge_index[:,5] =np.array([[1,2]])
# data.edge_index[:,6] =np.array([[2,1]])
# data.edge_index[:,7] =np.array([[2,3]])
# data.edge_index[:,8] =np.array([[3,2]])
# data.edge_index[:,9] =np.array([[3,4]])
# data.edge_index[:,10] =np.array([[4,3]])
# data.edge_index[:,11] =np.array([[4,5]])
# data.edge_index[:,12] =np.array([[5,4]])
# data.edge_index[:,13] =np.array([[1,3]])
# data.edge_index[:,14] =np.array([[3,1]])
# data.edge_index[:,15] =np.array([[2,4]])
# data.edge_index[:,16] =np.array([[4,2]])
# data.edge_index[:,17] =np.array([[3,5]])
# data.edge_index[:,18] =np.array([[5,3]])
# data.edge_index[:,19] =np.array([[1,1]])
# data.edge_index[:,20] =np.array([[2,2]])
# data.edge_index[:,21] =np.array([[3,3]])
# data.edge_index[:,22] =np.array([[4,4]])
# data.edge_index[:,23] =np.array([[5,5]])

# data.edge_index =  torch.from_numpy(data.edge_index).long()

# edge_attr = np.zeros((data.n_edges, data.relation_dim))
# edge_attr[0,0]= 1
# edge_attr[1,1]= 1
# edge_attr[2,2]= 1
# edge_attr[3,3]= 1
# edge_attr[4,4]= 1
# edge_attr[5,5]= edge_attr[6,5]= edge_attr[7,5]= edge_attr[8,5]=  edge_attr[9,5] = edge_attr[10,5]= edge_attr[11,5]= edge_attr[12,5]= 1
# edge_attr[13,6]= edge_attr[14,6]= edge_attr[15,6]= edge_attr[16,6]=  edge_attr[17,6] = edge_attr[18,6] = 1
# edge_attr[19,7]= edge_attr[20,7]= edge_attr[21,7]= edge_attr[22,7]=  edge_attr[23,7] = 1
# # data.x_cat = torch.cat([data.states, data.attr], -1)
#
# print(data)

class Data_states:
    pass 
class Data_g:
    pass 
# class args_general:
#     pass

# # encoder, decoder argument class
# class args_e:
#     pass 
# class args2_e:
#     pass 
# class args_d:
#     pass 
# class args2_d:
#     pass 


# args_e.state_dim, args_e.attr_dim, args_e.action_dim, args_e.relation_dim = data.state_dim, data.attr_dim, data.action_dim, data.relation_dim
# args_e.nf_particle, args_e.nf_relation, args_e.nf_effect = 70, 70, 70  #100
# args_e.n_edges = data.n_edges
# args_e.output_dim = args_e.nf_particle

# # define arguments for the second layer
# g_dim = 32  #32
# args2_e.state_dim, args2_e.attr_dim, args2_e.action_dim, args2_e.relation_dim = args_e.nf_particle, data.attr_dim, data.action_dim, data.relation_dim
# args2_e.nf_particle, args2_e.nf_relation, args2_e.nf_effect = g_dim, 70, 70
# args2_e.n_edges = data.n_edges
# args2_e.output_dim = args2_e.nf_particle

# # decoder
# args_d.state_dim, args_d.attr_dim, args_d.action_dim, args_d.relation_dim = args2_e.output_dim, data.attr_dim, data.action_dim, data.relation_dim
# args_d.nf_particle, args_d.nf_relation, args_d.nf_effect = args_e.state_dim, 70, 70
# args_d.n_edges = data.n_edges
# args_d.output_dim = args_d.nf_particle

# # define arguments for the second layer of decoder
# args2_d.state_dim, args2_d.attr_dim, args2_d.action_dim, args2_d.relation_dim = args_d.nf_particle, data.attr_dim, data.action_dim, data.relation_dim
# args2_d.nf_particle, args2_d.nf_relation, args2_d.nf_effect = args_e.state_dim, 70, 70
# args2_d.n_edges = data.n_edges
# args2_d.output_dim = args_d.nf_particle



'''
model
'''
# build model
use_gpu = torch.cuda.is_available()
if not args.baseline:
    """ Koopman model"""
    #model = CompositionalKoopmanOperators(args, residual=False, use_gpu=use_gpu)
    model = GNNKoopman(args_e, args2_e, args_d, args2_d, action = True, tanh = True, residual=False, use_gpu=use_gpu)


    # load pretrained checkpoint
    if args.eval_epoch == -1:
        model_path = os.path.join(args.outf, 'net_best.pth')
    else:
        model_path = os.path.join(args.outf, 'net_epoch_%d_iter_%d.pth' % (args.eval_epoch, args.eval_iter))
    
    print("Loading saved checkpoint from %s" % model_path)
    device = torch.device('cuda:0') if use_gpu else torch.device('cpu')
    model.load_state_dict(torch.load(model_path,map_location=device))
    model.eval()
    if use_gpu: model.cuda()

else:
    """ Koopman Baselinese """
    model = KoopmanBaseline(args)

'''
eval
'''


def get_more_trajectories(roll_idx):
    group_idx = roll_idx // args.group_size
    offset = group_idx * args.group_size

    all_seq = [[], [], [], []]

    for i in range(1, args.fit_num + 1):
        new_idx = (roll_idx + i - offset) % args.group_size + offset
        seq_data = load_data(prepared_names, os.path.join(data_dir, str(new_idx) + '.rollout.h5'))
        for j in range(4):
            all_seq[j].append(seq_data[j])

    all_seq = [np.array(all_seq[j], dtype=np.float32) for j in range(4)]
    return all_seq

def eval(idx_rollout, video=True):
    print(f'\n=== Forward Simulation on Example {roll_idx} ===')

    seq_data = load_data(prepared_names, os.path.join(data_dir, str(idx_rollout) + '.rollout.h5'))
    attrs, states, actions, rel_attrs = seq_data
    #attrs, states, actions, rel_attrs = torch.tensor(attrs), torch.tensor(states), torch.tensor(actions), torch.tensor(rel_attrs)
    #rel_attrs= edge_attr[None,:,:].repeat(np.shape(rel_attrs)[0], axis=0)
    #seq_data = [attrs, states, actions, rel_attrs]        


    attrs, states, actions, rel_attrs = [to_var(d.copy(), use_gpu=use_gpu) for d in seq_data]

    seq_data = denormalize(seq_data, stat)
    attrs_gt, states_gt, action_gt = seq_data[:3]

    # param_file = os.path.join(data_dir, str(idx_rollout // args.group_size) + '.param')
    # param = torch.load(param_file)
    # engine.init(param)

    '''
    fit data
    '''
    fit_data = get_more_trajectories(roll_idx)
    attrs_2, states_2, actions_2, rel_attrs_2 = fit_data
    #attrs_2, states_2, actions_2, rel_attrs_2 = torch.tensor(attrs_2), torch.tensor(states_2), torch.tensor(actions_2), torch.tensor(rel_attrs_2)
    
    #rel_attrs_2= edge_attr[None,:,:].repeat(np.shape(rel_attrs_2)[1], axis=0)[None,:,:,:].repeat(np.shape(rel_attrs_2)[0], axis=0)
    #fit_data = [attrs_2, states_2, actions_2, rel_attrs_2]


    fit_data = [to_var(d, use_gpu=use_gpu) for d in fit_data]
    bs = args.fit_num

    ''' T x N x D (denormalized)'''
    states_pred = states_gt.copy()
    states_pred[1:] = 0

    ''' T x N x D (normalized)'''
    s_pred = states.clone()

    '''
    reconstruct loss
    '''
    attrs_flat = get_flat(fit_data[0])
    states_flat = get_flat(fit_data[1])
    actions_flat = get_flat(fit_data[2])
    rel_attrs_flat = get_flat(fit_data[3])

    # 
    Data_states.states = states_flat
    Data_states.n_nodes = states_2.shape[2]
    Data_states.n_edges = rel_attrs_2.shape[2]
    Data_states.state_dim = states_2.shape[3]
    Data_states.attr_dim = attrs_2.shape[3]
    Data_states.attr = attrs_flat
    Data_states.action_dim = actions_2.shape[3]
    Data_states.action = actions_flat

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    if use_gpu:
        data.edge_index = data.edge_index.to(device)


    Data_states.edge_index = data.edge_index  # need to define this globally
    Data_states.x_cat = torch.cat([Data_states.states, Data_states.attr], 2)

    Data_states.relation_dim = rel_attrs_2.shape[2]
    Data_states.edge_attr = rel_attrs_flat

    #bs = len(attrs)
    g = model.to_g(Data_states)
    g = g.view(torch.Size([bs, args.time_step]) + g.size()[1:])

    G_tilde = g[:, :-1]
    H_tilde = g[:, 1:]
    U_tilde = fit_data[2][:, :-1]

    G_tilde = get_flat(G_tilde, keep_dim=True)
    H_tilde = get_flat(H_tilde, keep_dim=True)
    U_tilde = get_flat(U_tilde, keep_dim=True)

    _t = time.time()
    # A, B, fit_err = model.system_identify_distributed(
    #     G=G_tilde, H=H_tilde, U=U_tilde, rel_attrs=fit_data[3][:1, 0], I_factor=args.I_factor)

    A, B, fit_err = model.system_identify_distributed(G=G_tilde, H=H_tilde, U=U_tilde,
                                                        rel_attrs=rel_attrs[:1, 0], I_factor=args.I_factor, neighbour_self_num=neighbour_self_num,
                                                        g_d=g_d, Tfp_list=Tfp_list, Rfp_list=Rfp_list, Tpp_list=Tpp_list, Rpp_list=Rpp_list, Tup_list=Tup_list, Rup_list=Rup_list)
                
    _t = time.time() - _t

    '''
    predict
    '''
    Data_states.states = states
    Data_states.attr = attrs
    Data_states.action = actions
    Data_states.x_cat = torch.cat([Data_states.states, Data_states.attr], 2)

    
    Data_states.edge_attr = rel_attrs
    g = model.to_g(Data_states)

    pred_g = None
    for step in range(0, args.time_step - 1):
        # prepare input data

        if step == 0:
            current_s = states[step:step + 1]
            current_g = g[step:step + 1]
            states_pred[step] = states_gt[step]
        else:
            '''current state'''
            if args.eval_type == 'valid':
                current_s = states[step:step + 1]
            elif args.eval_type == 'rollout':
                current_s = s_pred[step:step + 1]

            '''current g'''
            if args.eval_type in {'valid', 'rollout'}:
                Data_states.states = current_s
                Data_states.attr = attrs[step:step + 1]
                Data_states.x_cat = torch.cat([Data_states.states, Data_states.attr], 2)
                Data_states.edge_attr = rel_attrs[step:step + 1]
                current_g = model.to_g(Data_states)
            elif args.eval_type == 'koopman':
                current_g = pred_g

        '''next g'''
        pred_g = model.step(g=current_g, u=actions[step:step + 1], rel_attrs=rel_attrs[step:step + 1])

        '''decode s'''
        # set Data_g
        Data_g.states = pred_g
        Data_g.n_nodes = states_2.shape[2]
        Data_g.n_edges = rel_attrs_2.shape[2]
        Data_g.state_dim = pred_g.shape[1]
        Data_g.attr_dim = attrs_2.shape[3]
        Data_g.attr = attrs[step:step + 1]
        Data_g.edge_index = data.edge_index  # need to define this globally
        Data_g.x_cat = torch.cat([Data_g.states, Data_g.attr], 2)

        Data_g.relation_dim = rel_attrs_2.shape[3]
        Data_g.edge_attr = rel_attrs[step:step + 1]

        #pred_s = model.to_s(attrs=attrs[step:step + 1], gcodes=pred_g,
        #                    rel_attrs=rel_attrs[step:step + 1], pstep=args.pstep)

        pred_s = model.to_s(Data_g)

        #pred_s_np_denorm = denormalize([to_np(pred_s)], [stat[1]])[0]

        states_pred[step + 1:step + 2] = to_np(pred_s)
        d = args.state_dim // 2
        states_pred[step + 1:step + 2, :, :d] = states_pred[step:step + 1, :, :d] + \
                                                args.dt * states_pred[step + 1:step + 2, :, d:]

        #s_pred_next = normalize([states_pred[step + 1:step + 2]], [stat[1]])[0]
        s_pred_next = states_pred[step + 1:step + 2]
        s_pred[step + 1:step + 2] = to_var(s_pred_next, use_gpu=use_gpu)

    if video:
        engine.render(states_pred, seq_data[2], param, act_scale=args.act_scale, video=False, image=True,
                      path=os.path.join(args.evalf, str(idx_rollout) + '.pred'),
                      states_gt=states_gt)
        
        loss = torch.nn.MSELoss(reduction='none')
        loss_result = torch.mean(torch.mean(loss(torch.Tensor(states_pred),torch.Tensor(states_gt)),dim=1), dim=1)
        plt.plot(loss_result)
        plt.savefig(os.path.join(args.evalf, 'Loss_Accross_time_allsamples.png'))

        return loss_result

if __name__ == '__main__':

    # num_train = int(args.n_rollout * args.train_valid_ratio)
    # num_valid = args.n_rollout - num_train

    ls_rollout_idx = np.arange(0, 10)
    #ls_rollout_idx[1] = 356
    if args.demo:
        ls_rollout_idx = np.arange(8) * 25

    loss = []
    states_pred_test = []
    states_gt_test = []
    
    for roll_idx in ls_rollout_idx:
        
        loss_id, states_pred, states_gt = eval(roll_idx)
        states_pred_test.append(states_pred)
        states_gt_test.append(states_gt)
        loss.append(loss_id)

    loss = torch.mean(torch.stack(loss),dim=0)
    savemat(os.path.join(args.evalf, 'oscillator_dist_high_nd'), {'states_pred_test': states_pred_test, 'states_gt_test': states_gt_test})
    plt.figure(2)
    plt.plot(loss)
    plt.savefig(os.path.join(args.evalf, 'Loss_Accross_time_avg.png'))